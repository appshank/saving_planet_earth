package com.appshank.spe.services;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.Profile;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Base64Coder;
import com.badlogic.gdx.utils.Json;

public class ProfileManager {

	// the location of the profile data file
	private static final String PROFILE_DATA_FILE = ".spe/profile-v1.json";

	// the loaded profile (may be null)
	private Profile profile;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.appshank.tyrian.services.ProfileManager#persist()
	 */
	public void persist() {
		if (profile != null) {
			persist(profile);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.appshank.tyrian.services.ProfileManager#persist(Profile profile)
	 */
	public void persist(Profile profile) {
		// create the handle for the profile data file
		FileHandle profileDataFile = null;
		if (Gdx.app.getType() == ApplicationType.WebGL) {
			profileDataFile = Gdx.files.internal(PROFILE_DATA_FILE);
		} else {
			profileDataFile = Gdx.files.local(PROFILE_DATA_FILE);
		}
		Gdx.app.log(SavingPlanetEarth.LOG, "Persisting profile in: "
				+ profileDataFile.path());

		// convert the profile to text
		if (Gdx.app.getType() != ApplicationType.WebGL) {
			// TODO : find a way to persist profiles of online players

			// create the JSON utility object
			Json json = new Json();

			String profileAsText = json.toJson(profile);

			Gdx.app.log(SavingPlanetEarth.LOG, "PROFILE => " + profileAsText);
			
			// encode the text
			if (!SavingPlanetEarth.DEV_MODE) {
				profileAsText = Base64Coder.encodeString(profileAsText);
			}

			// write the encoded text to the file
			profileDataFile.writeString(profileAsText, false);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.appshank.tyrian.services.ProfileManager#retrieveProfile()
	 */
	public Profile retrieveProfile() {
		Gdx.app.log(SavingPlanetEarth.LOG, "Retrieving Profile");

		// if profile is already loaded just return it
		if (profile != null)
			return profile;

		// create the handle for the profile data file
		FileHandle profileDataFile = null;
		if (Gdx.app.getType() == ApplicationType.WebGL) {
			profileDataFile = Gdx.files.internal(PROFILE_DATA_FILE);
		} else {
			profileDataFile = Gdx.files.local(PROFILE_DATA_FILE);
		}

		// create the JSON utility object
		Json json = new Json();

		// check if the profile data file exists
		if (profileDataFile.exists()) {
			// load the profile from the data file
			try {
				// read the file as text
				String profileAsText = profileDataFile.readString().trim();

				// decode the contents (if it's base64 encoded)
				if (profileAsText.matches("^[A-Za-z0-9/+=]+$")) {
					Gdx.app.log(SavingPlanetEarth.LOG,
							"Persisted profile is base64 encoded");
					profileAsText = Base64Coder.decodeString(profileAsText);
				}

				// restore the state
				profile = json.fromJson(Profile.class, profileAsText);
			} catch (Exception e) {
				// log the exception
				Gdx.app.error(SavingPlanetEarth.LOG,
						"Unable to parse existing profile data file", e);

				// recover by creating a fresh new profile data file;
				// note that the player will lose all game progress
				profile = new Profile();
				persist(profile);
			}

		} else {
			// create a new profile data file
			profile = new Profile();
			persist(profile);
		}

		// return the result
		return profile;

	}
}
