package com.appshank.spe.services;

import java.nio.IntBuffer;

import com.appshank.spe.SavingPlanetEarth;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.ResolutionFileResolver;
import com.badlogic.gdx.assets.loaders.resolvers.ResolutionFileResolver.Resolution;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.TimeUtils;

public class MyAssetsManager implements AssetErrorListener {

	private AssetManager manager;

	private long start;

	public static final String FONT_FOLDER = "fonts/";
	public static final String COUNTER_FONT = "counter";
	public static final String DEFAULT_FONT = "carter_one_32";
	public static final String GAME_OVER_FONT = "carter_one_32";
	public static final String SCREEN_TITLE_FONT = "carter_one_32";
	public static final String GAME_TITLE_FONT = "carter_one_64_game_title";

	// do the initialization stuff
	public void init() {
		Gdx.app.log(SavingPlanetEarth.LOG, "inside MyAssetsManager.init()");
		Resolution resolutionBase = new Resolution(480, 800, "480800");
		Resolution resolution6001024 = new Resolution(600, 1024, "6001024");
		Resolution resolution8001280 = new Resolution(800, 1280, "8001280");
		Resolution[] resolutions = { resolutionBase, resolution6001024,
				resolution8001280 };
		ResolutionFileResolver resolutionFileResolver = new ResolutionFileResolver(
				new InternalFileHandleResolver(), resolutions);

		System.out.println("Asset path: "
				+ resolutionFileResolver.resolve("image-atlases/pages.atlas")
						.path());

		manager = new AssetManager();
		manager.setLoader(TextureAtlas.class, new TextureAtlasLoader(
				resolutionFileResolver));
		manager.setErrorListener(this);

		load();

		Texture.setAssetManager(manager);

	}

	// load the textures
	// to be called after init()
	private void load() {
		start = TimeUtils.nanoTime();

		// atlas
		loadAtlas();

		// fonts
		loadFonts();

	}

	private void loadFonts() {
		// game title font
		loadFont(GAME_TITLE_FONT);

		// screen title font
		loadFont(SCREEN_TITLE_FONT);

		// gameover font
		loadFont(GAME_OVER_FONT);

		// default font
		loadFont(DEFAULT_FONT);

		// counter font
		loadFont(COUNTER_FONT);
	}

	private void loadFont(String fontTitle) {
		manager.load(FONT_FOLDER + fontTitle + ".fnt", BitmapFont.class);
	}

	private void loadAtlas() {
		manager.load("image-atlases/pages.atlas", TextureAtlas.class);
	}

	private void unload() {
		unloadAtlas();
		unloadFonts();
	}

	private void unloadFonts() {
		// game title font
		unloadFont(GAME_TITLE_FONT);

		// screen title font
		unloadFont(SCREEN_TITLE_FONT);

		// gameover font
		unloadFont(GAME_OVER_FONT);

		// default font
		unloadFont(DEFAULT_FONT);

		// counter font
		unloadFont(COUNTER_FONT);
	}

	private void unloadFont(String fontTitle) {
		manager.unload(FONT_FOLDER + fontTitle);
	}

	private void unloadAtlas() {
		manager.unload("image-atlases/pages.atlas");
	}

	private void invalidateTexture(Texture texture) {
		IntBuffer buffer = BufferUtils.newIntBuffer(1);
		buffer.put(0, texture.getTextureObjectHandle());
		Gdx.gl.glDeleteTextures(1, buffer);
	}

	@Override
	public void error(String fileName, Class type, Throwable throwable) {
		Gdx.app.error("MyAssetManager : ", "couldn't load asset '" + fileName
				+ "'", (Exception) throwable);

	}

	public void dispose() {
		manager.dispose();
	}

	public <T> T get(String fileName, Class<T> type) {
		if (manager.isLoaded(fileName, type))
			return manager.get(fileName, type);
		return null;
	}

	public boolean update() {
		return manager.update();
	}

	public long getStartTime() {
		return start;
	}

	public float getProgress() {
		return manager.getProgress();
	}

	public BitmapFont getFont(String fontTitle) {
		return get(FONT_FOLDER + fontTitle + ".fnt", BitmapFont.class);
	}
}
