package com.appshank.spe.services;

import com.appshank.spe.SavingPlanetEarth;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class PreferencesManager {

	// constants
	private static final String PREF_VOLUME = "volume";
	private static final String PREF_MUSIC_ENABLED = "music.enabled";
	private static final String PREF_SOUND_ENABLED = "sound.enabled";
	private static final String PREF_VIBRATION_ENABLED = "vibration.enabled";
	private static final String PREFS_NAME = "spe";

	protected Preferences getPrefs() {
		return Gdx.app.getPreferences(PREFS_NAME);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.appshank.tyrian.services.impl.tmp#isSoundEnabled()
	 */
	public boolean isSoundEnabled() {
		return getPrefs().getBoolean(PREF_SOUND_ENABLED, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.appshank.tyrian.services.impl.tmp#setSoundEnabled(boolean)
	 */
	public void setSoundEnabled(boolean soundEffectsEnabled) {
		getPrefs().putBoolean(PREF_SOUND_ENABLED, soundEffectsEnabled);
		getPrefs().flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.appshank.tyrian.services.impl.tmp#isMusicEnabled()
	 */
	public boolean isMusicEnabled() {
		return getPrefs().getBoolean(PREF_MUSIC_ENABLED, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.appshank.tyrian.services.impl.tmp#setMusicEnabled(boolean)
	 */
	public void setMusicEnabled(boolean musicEnabled) {
		getPrefs().putBoolean(PREF_MUSIC_ENABLED, musicEnabled);
		getPrefs().flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.appshank.tyrian.services.impl.tmp#getVolume()
	 */
	public float getVolume() {
		return getPrefs().getFloat(PREF_VOLUME, 0.5f);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.appshank.tyrian.services.impl.tmp#setVolume(float)
	 */
	public void setVolume(float volume) {
		getPrefs().putFloat(PREF_VOLUME, volume);
		getPrefs().flush();
	}

	public boolean isVibrationEnabled() {
		return getPrefs().getBoolean(PREF_VIBRATION_ENABLED, true);
	}

	public void setVibrationEnabled(boolean vibrationEnabled) {
		Gdx.app.log(SavingPlanetEarth.LOG,
				"inside setVibrationEnabled(): setting vibrationEnabled to "
						+ vibrationEnabled);
		getPrefs().putBoolean(PREF_VIBRATION_ENABLED, vibrationEnabled);
		getPrefs().flush();
	}

}
