package com.appshank.spe.services;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.services.SoundManager.SPESound;
import com.appshank.spe.utils.LRUCache;
import com.appshank.spe.utils.LRUCache.CacheEntryRemovedListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;

/**
 * A service that manages the sound effects.
 */
public class SoundManager implements
		CacheEntryRemovedListener<SPESound, Sound>, Disposable {

	/**
	 * The available sound files.
	 */
	public enum SPESound {
		CLICK("sound/click.wav");

		private final String fileName;

		private SPESound(String fileName) {
			this.fileName = fileName;
		}

		public String getFileName() {
			return fileName;
		}
	}

	/**
	 * The volume to be set on the sound.
	 */
	private float volume = 1f;

	/**
	 * Whether the sound is enabled.
	 */
	private boolean enabled = true;

	/**
	 * The sound cache.
	 */
	private final LRUCache<SPESound, Sound> soundCache;

	/**
	 * Creates the sound manager.
	 */
	public SoundManager() {
		soundCache = new LRUCache<SPESound, Sound>(10);
		soundCache.setEntryRemovedListener(this);
	}

	/**
	 * Plays the specified sound.
	 */
	public void play(SPESound sound) {
		// check if the sound is enabled
		if (!enabled)
			return;

		// try and get the sound from the cache
		Sound soundToPlay = soundCache.get(sound);
		if (soundToPlay == null) {
			FileHandle soundFile = Gdx.files.internal(sound.getFileName());
			soundToPlay = Gdx.audio.newSound(soundFile);
			soundCache.add(sound, soundToPlay);
		}

		// play the sound
		Gdx.app.log(SavingPlanetEarth.LOG, "Playing sound: " + sound.name());
		soundToPlay.play(volume);
	}

	/**
	 * Sets the sound volume which must be inside the range [0,1].
	 */
	public void setVolume(float volume) {
		Gdx.app.log(SavingPlanetEarth.LOG, "Adjusting sound volume to: "
				+ volume);

		// check and set the new volume
		if (volume < 0 || volume > 1f) {
			throw new IllegalArgumentException(
					"The volume must be inside the range: [0,1]");
		}
		this.volume = volume;
	}

	/**
	 * Enables or disabled the sound.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	// EntryRemovedListener implementation

	@Override
	public void notifyEntryRemoved(SPESound key, Sound value) {
		Gdx.app.log(SavingPlanetEarth.LOG, "Disposing sound: " + key.name());
		value.dispose();
	}

	@Override
	public void dispose() {
		Gdx.app.log(SavingPlanetEarth.LOG, "Disposing sound manager");
		for (Sound sound : soundCache.retrieveAll()) {
			sound.stop();
			sound.dispose();
		}
	}

}
