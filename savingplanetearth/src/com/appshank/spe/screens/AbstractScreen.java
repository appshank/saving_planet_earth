package com.appshank.spe.screens;

import com.appshank.spe.SavingPlanetEarth;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public abstract class AbstractScreen implements Screen {

	// the fixed viewport dimensions (ratio: 1.6)
	public static final int GAME_VIEWPORT_WIDTH = 400;
	public static final int GAME_VIEWPORT_HEIGHT = 240;
	protected static final int MENU_VIEWPORT_WIDTH = 800,
			MENU_VIEWPORT_HEIGHT = 480;

	protected final SavingPlanetEarth game;
	protected final Stage stage;

	// private SpriteBatch batch;
	private BitmapFont font;
	private Skin skin;
	protected static TextureAtlas atlas;

	static final int SPACE_ABOVE_BUTTONS = 15;

	public AbstractScreen(SavingPlanetEarth game) {
		this.game = game;
		int width = isGameScreen() ? GAME_VIEWPORT_WIDTH : MENU_VIEWPORT_WIDTH;
		int height = isGameScreen() ? GAME_VIEWPORT_HEIGHT
				: MENU_VIEWPORT_HEIGHT;

		Gdx.app.log(SavingPlanetEarth.LOG, "Gdx.graphics.getWidth() : "
				+ Gdx.graphics.getWidth() + ", Gdx.graphics.getHeight() : "
				+ Gdx.graphics.getHeight());

		this.stage = new Stage(width, height, true);
	}

	protected boolean isGameScreen() {
		return false;
	}

	protected abstract String getName();

	// Lazily loaded collaborators

	public BitmapFont getFont() {
		if (font == null) {
			font = getSkin().getFont("default-font");
		}
		return font;
	}

	/*
	 * public SpriteBatch getBatch() { if (batch == null) { batch =
	 * stage.getSpriteBatch(); } return batch; }
	 */
	public static TextureAtlas getAtlas() {
		return atlas;
	}

	protected Skin getSkin() {
		if (skin == null) {
			FileHandle skinFile = Gdx.files.internal("skin/uiskin.json");
			skin = new Skin(skinFile);

		}
		return skin;
	}

	Table addTableActor(Skin skin) {
		Table table = new Table(skin);
		stage.addActor(table);
		table.setFillParent(true);
		AtlasRegion atlasRegion = getAtlas().findRegion(
				"menu-screen/background-2");
		table.setBackground(new TextureRegionDrawable(atlasRegion));
		table.columnDefaults(0).padRight(20);
		return table;
	}

	void addCreditsLabel(int width, int height) {
		Label creditsLabel = new Label("", getSkin());
		int credits = game.getProfileManager().retrieveProfile().getCredits();
		creditsLabel.setText("Credits : " + Integer.toString(credits));
		stage.addActor(creditsLabel);
		creditsLabel.setPosition(width - 165, height - 15);
		creditsLabel.setWidth(160f);
		creditsLabel.setAlignment(Align.right);
	}

	@Override
	public void render(float delta) {
		// (1) process the game logic

		// update the actors
		stage.act(delta);

		// (2) draw the result

		// clear the screen with the given RGB color (black)
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// draw the actors
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log(SavingPlanetEarth.LOG, "Resizing screen: " + getName()
				+ " to: " + width + " x " + height);

		// resize the stage (no need for resizing as we have defined our
		// viewport dimensions
		stage.setViewport(width, height, true);
	}

	@Override
	public void hide() {
		Gdx.app.log(SavingPlanetEarth.LOG, "Hiding screen: " + getName());

		// dispose the screen when leaving the screen;
		// note that the dipose() method is not called automatically by the
		// framework, so we must figure out when it's appropriate to call it
		dispose();
	}

	@Override
	public void pause() {
		Gdx.app.log(SavingPlanetEarth.LOG, "Pausing screen: " + getName());

	}

	@Override
	public void resume() {
		Gdx.app.log(SavingPlanetEarth.LOG, "Resuming screen: " + getName());

	}

	@Override
	public void show() {
		Gdx.app.log(SavingPlanetEarth.LOG, "Showing screen: " + getName());

		// set the stage as the input processor
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void dispose() {
		Gdx.app.log(SavingPlanetEarth.LOG, "Disposing screen: " + getName());

		// dispose the collaborators
		// stage.dispose();

		// as the collaborators are lazily loaded, they may be null
		if (font != null)
			font.dispose();
		// if (batch != null)
		// batch.dispose();
		if (skin != null)
			skin.dispose();
	}
}
