package com.appshank.spe.screens;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.Profile;
import com.appshank.spe.domain.factories.FontFactory;
import com.appshank.spe.services.SoundManager.SPESound;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.esotericsoftware.tablelayout.BaseTableLayout;

/**
 * A simple high scores screen.
 */
public class HighScoresScreen extends AbstractScreen {

	private Table table;

	public HighScoresScreen(SavingPlanetEarth game) {
		super(game);
	}

	@Override
	public void show() {
		super.show();

		// retrieve the skin created on the AbstractScreen class
		Skin skin = super.getSkin();

		// get the profile
		Profile profile = game.getProfileManager().retrieveProfile();

		// button "Back to Menu"
		TextButton mainMenuButton = new TextButton("Back to Main Menu", skin);
		mainMenuButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log(SavingPlanetEarth.LOG, "mainMenuButton clicked");
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);
				game.setScreen(new MenuScreen(game));
			}
		});

		// create the table actor
		table = addTableActor(skin);

		// add the welcome message with a margin-bottom of 50 units
		LabelStyle screenTitleStyle = new LabelStyle(
				FontFactory.getScreenTitleFont(), Color.WHITE);
		Label screenTitleLabel = new Label("HIGH SCORES", screenTitleStyle);
		table.add(screenTitleLabel).colspan(2);

		// add the highscores
		Array<Integer> highScores = profile.getHighScores();
		int numHighScores = highScores.size;
		for (int i = 0; i < numHighScores; i++) {
			table.row();
			if (i == numHighScores - 1) {
				table.add(highScores.get(i).toString()).colspan(2)
						.align(BaseTableLayout.CENTER)
						.spaceBottom(SPACE_ABOVE_BUTTONS);
			} else {
				table.add(highScores.get(i).toString()).colspan(2)
						.align(BaseTableLayout.CENTER);
			}

		}

		// add the start-game button sized 300x60 with a margin-bottom of 10
		// units
		table.row();
		table.add(mainMenuButton).size(350, 60).colspan(2);
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		addCreditsLabel(width, height);
	}

	@Override
	protected String getName() {
		return "HighScoresScreen";
	}
}
