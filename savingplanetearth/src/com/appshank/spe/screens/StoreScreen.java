package com.appshank.spe.screens;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.BonusPack;
import com.appshank.spe.domain.ColorPaletter16Pal;
import com.appshank.spe.domain.Profile;
import com.appshank.spe.domain.factories.BonusPackFactory;
import com.appshank.spe.domain.factories.FontFactory;
import com.appshank.spe.services.SoundManager.SPESound;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

public class StoreScreen extends AbstractScreen {

	private Table table;
	private Label healthPackUpdatesPurchased;
	private Label starPackUpdatesPurchased;
	private Label shieldPackUpdatesPurchased;
	private Label creditsLabel;

	private int credits;

	// the profile
	Profile profile;
	// get the purchased bonus packs
	Array<Integer> purchasedBonusPackTypes;
	// get the levels of the purchased bonus packs
	Array<Integer> purchasedBonusPackLevels;

	public StoreScreen(SavingPlanetEarth game) {
		super(game);
	}

	private void decreaseCreditCountOnLabel() {
		// update the creditsLabel accordingly
		creditsLabel.setText("Credits : " + Integer.toString(credits));
	}

	@Override
	public void show() {
		super.show();

		// cache the number of credits in the profile
		profile = game.getProfileManager().retrieveProfile();
		credits = profile.getCredits();
		purchasedBonusPackTypes = profile.getPurchasedBonusPacks();
		purchasedBonusPackLevels = profile.getPurchasedBonusPackLevels();

		// retrieve the skin created on the AbstractScreen class
		Skin skin = super.getSkin();

		// create the table actor
		table = addTableActor(skin);

		LabelStyle screenTitleStyle = new LabelStyle(
				FontFactory.getScreenTitleFont(), ColorPaletter16Pal.BLAZE);
		Label screenTitleLabel = new Label("STORE", screenTitleStyle);
		table.add(screenTitleLabel).colspan(3);

		// add the HealthPack label
		table.row();
		healthPackUpdatesPurchased = new Label("", skin);
		final TextButton increaseHealthPackFrequencyButton = new TextButton(
				"+", skin);
		increaseHealthPackFrequencyButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);

				// increase the value for health pack frequency label
				increaseHealthPackUpdatesPurchasedLabel();

				// decrease the credit count being displayed
				decreaseCreditCountOnLabel();
			}

			private void increaseHealthPackUpdatesPurchasedLabel() {
				// check if there are enough credits to buy the upgrade
				if (credits > BonusPackFactory.getBonusPack(BonusPack.HEALTH)
						.getUpgradeCost()) {
					// in case there are enough credits, upgrade the level and
					// decrease the number of credits accordingly
					upgradeLevel(BonusPack.HEALTH);
				} else {
					// TODO: else display the HUD label "not enough credits"
				}

			}

		});
		table.add("Health Pack");
		table.add(healthPackUpdatesPurchased).center();
		table.add(increaseHealthPackFrequencyButton);

		// add the StarPack label
		table.row();
		starPackUpdatesPurchased = new Label("", skin);
		final TextButton increaseStarPackFrequencyButton = new TextButton("+",
				skin);
		increaseStarPackFrequencyButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);

				// increase the value for health pack frequency label
				increaseStarPackUpdatesPurchasedLabel();

				// decrease the credit count being displayed
				decreaseCreditCountOnLabel();
			}

			private void increaseStarPackUpdatesPurchasedLabel() {
				// check if there are enough credits to buy the upgrade
				if (credits > BonusPackFactory.getBonusPack(BonusPack.STAR)
						.getUpgradeCost()) {
					// in case there are enough credits, upgrade the level and
					// decrease the number of credits accordingly
					upgradeLevel(BonusPack.STAR);
				} else {
					// TODO: else display the HUD label "not enough credits"
				}

			}

		});
		table.add("Star Pack");
		table.add(starPackUpdatesPurchased).center();
		table.add(increaseStarPackFrequencyButton);

		// add the ShieldPack label
		table.row();
		shieldPackUpdatesPurchased = new Label("", skin);
		final TextButton increaseShieldPackFrequencyButton = new TextButton(
				"+", skin);
		increaseShieldPackFrequencyButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);

				// increase the value for health pack frequency label
				increaseShieldPackUpdatesPurchasedLabel();

				// decrease the credit count being displayed
				decreaseCreditCountOnLabel();
			}

			private void increaseShieldPackUpdatesPurchasedLabel() {
				// check if there are enough credits to buy the upgrade
				if (credits > BonusPackFactory.getBonusPack(BonusPack.SHIELD)
						.getUpgradeCost()) {
					// in case there are enough credits, upgrade the level and
					// decrease the number of credits accordingly
					upgradeLevel(BonusPack.SHIELD);
				} else {
					// TODO: else display the HUD label "not enough credits"
				}

			}

		});
		table.add("Shield Pack");
		table.add(shieldPackUpdatesPurchased).center();
		table.add(increaseShieldPackFrequencyButton).spaceBottom(
				SPACE_ABOVE_BUTTONS);

		// next row
		table.row();
		// button "Back to Menu"
		TextButton mainMenuButton = new TextButton("Back to Main Menu", skin);
		mainMenuButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log(SavingPlanetEarth.LOG, "mainMenuButton clicked");
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);
				game.setScreen(new MenuScreen(game));
			}
		});
		table.add(mainMenuButton).size(350, 60).colspan(1);

		// button "Accept Updates"
		TextButton acceptUpdatesButton = new TextButton("Accept Updates", skin);
		acceptUpdatesButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log(SavingPlanetEarth.LOG, "mainMenuButton clicked");
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);
				// update the changes from bonusPackUpdate labels to the profile
				updateProfileForChanges();
				// go back to MenuScreen
				game.setScreen(new MenuScreen(game));
			}

		});
		table.add(acceptUpdatesButton).size(300, 60).colspan(2);

		// initialize the values for the bonus pack updates state
		initializeBonusPackUpdatesPurchased();

	}

	private void updateProfileForChanges() {
		// update the credits on the profile to the new value
		profile.setCredits(credits);

		// update the levels on the profile as per the current levels
		for (int bonusPackType : purchasedBonusPackTypes) {
			updateLevelInProfile(bonusPackType);
		}

	}

	private void updateLevelInProfile(int bonusPackType) {
		// get the index of this bonus pack type
		int bonusPackIndex = purchasedBonusPackTypes.indexOf(bonusPackType,
				false);

		// find the label for this bonus pack type
		Label labelAsPerBonusPackType = getLabelAsPerBonusPackType(bonusPackType);

		// retrieve the level for this bonus pack type from this label
		int newLevel = getLevelFromLabel(labelAsPerBonusPackType);

		// set the level in the levels array
		purchasedBonusPackLevels.set(bonusPackIndex, newLevel);
	}

	private int getLevelFromLabel(Label labelAsPerBonusPackType) {
		// get the current level from the label
		String labelText = labelAsPerBonusPackType.getText().toString();
		String[] labelTextParts = labelText.split("/");
		int newLevel = Integer.parseInt(labelTextParts[0]);
		return newLevel;
	}

	private void initializeBonusPackUpdatesPurchased() {
		// TODO

		// health pack
		int healthPackIndex = purchasedBonusPackTypes.indexOf(BonusPack.HEALTH,
				false);
		int healthPackLevel = purchasedBonusPackLevels.get(healthPackIndex);
		healthPackUpdatesPurchased.setText(healthPackLevel + "/10");

		// star pack
		int starPackIndex = purchasedBonusPackTypes.indexOf(BonusPack.STAR,
				false);
		int starPackLevel = purchasedBonusPackLevels.get(starPackIndex);
		starPackUpdatesPurchased.setText(starPackLevel + "/10");

		// shield pack
		int shieldPackIndex = purchasedBonusPackTypes.indexOf(BonusPack.SHIELD,
				false);
		int shieldPackLevel = purchasedBonusPackLevels.get(shieldPackIndex);
		shieldPackUpdatesPurchased.setText(shieldPackLevel + "/10");
	}

	private void upgradeLevel(int bonusPackType) {
		Label labelToBeUpgraded = getLabelAsPerBonusPackType(bonusPackType);

		// get the current level from the label
		int currentLevel = Integer.valueOf(labelToBeUpgraded.getText()
				.toString().split("/")[0]);

		// upgrade the level
		if (currentLevel < 10) {
			currentLevel++;

			// update the text on the label
			labelToBeUpgraded.setText(currentLevel + "/10");

			// decrease the credits
			credits -= BonusPackFactory.getBonusPack(bonusPackType)
					.getUpgradeCost();
		}

	}

	private Label getLabelAsPerBonusPackType(int bonusPackType) {
		Label labelToBeReturned = null;
		if (bonusPackType == BonusPack.HEALTH) {
			labelToBeReturned = healthPackUpdatesPurchased;
		} else if (bonusPackType == BonusPack.STAR) {
			labelToBeReturned = starPackUpdatesPurchased;
		} else if (bonusPackType == BonusPack.SHIELD) {
			labelToBeReturned = shieldPackUpdatesPurchased;
		}
		return labelToBeReturned;
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		// add the credits label
		addCreditsLabel(width, height);
	}

	@Override
	protected String getName() {
		return "StoreScreen";
	}

}
