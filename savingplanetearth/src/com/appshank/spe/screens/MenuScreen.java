package com.appshank.spe.screens;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.ColorPaletter16Pal;
import com.appshank.spe.domain.factories.FontFactory;
import com.appshank.spe.services.SoundManager.SPESound;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class MenuScreen extends AbstractScreen {

	private Table table;
	private AtlasRegion backgroundRegion;

	public MenuScreen(SavingPlanetEarth game) {
		super(game);
	}

	@Override
	public void show() {
		super.show();

	}

	@Override
	public void render(float delta) {
		super.render(delta);
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		// retrieve the skin created on the AbstractScreen class
		Skin skin = super.getSkin();

		// button "start game"
		TextButton startGameButton = new TextButton("Start Game", skin);
		startGameButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log(SavingPlanetEarth.LOG, "stargGameButton clicked");
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);
				game.setScreen(new GameScreen(game));
			}
		});

		// button "options"
		TextButton optionsButton = new TextButton("Options", skin);
		optionsButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log(SavingPlanetEarth.LOG, "optionsButton clicked");
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);
				game.setScreen(new OptionsScreen(game));
			}
		});

		// button "hall of fame"
		TextButton hallOfFameButton = new TextButton("Hall of Fame", skin);
		hallOfFameButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log(SavingPlanetEarth.LOG, "hallOfFameButton clicked");
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);
				game.setScreen(new HighScoresScreen(game));
			}
		});

		// button "go to store"
		TextButton goToStoreButton = new TextButton("Go To Store", skin);
		goToStoreButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log(SavingPlanetEarth.LOG, "goToStoreButton clicked");
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);
				game.setScreen(new StoreScreen(game));
			}
		});

		// create the table actor
		table = new Table(skin);
		table.setFillParent(true);
		backgroundRegion = getAtlas().findRegion("menu-screen/background-2");
		// Gdx.app.log(SavingPlanetEarth.LOG, "atlasRegion: " + atlasRegion);
		table.setBackground(new TextureRegionDrawable(backgroundRegion));

		// add the table to the stage and retrieve its layout
		stage.addActor(table);

		// add the welcome message with a margin-bottom of 30 units
		LabelStyle gameTitleStyle = new LabelStyle(
				FontFactory.getGameTitleFont(), ColorPaletter16Pal.SLIMEGREEN);
		Label gameTitleLabel = new Label("SAVING PLANET EARTH", gameTitleStyle);
		table.add(gameTitleLabel).spaceBottom(30);

		// move to the next row
		table.row();

		// add the start-game button sized 300x60 with a margin-bottom of 10
		// units
		table.add(startGameButton).size(300, 60).uniform().spaceBottom(15);

		// move to the next row
		table.row();

		// add the options button in a cell similar to the start-game button's
		// cell
		table.add(optionsButton).size(300, 60).uniform().spaceBottom(15);

		// move to the next row
		table.row();

		// add the high-scores button in a cell similar to the start-game
		// button's cell
		table.add(hallOfFameButton).size(300, 60).uniform().spaceBottom(15);

		// move to the next row
		table.row();

		// add the high-scores button in a cell similar to the start-game
		// button's cell
		table.add(goToStoreButton).size(300, 60).uniform();

		// add the credits label
		addCreditsLabel(width, height);

	}

	

	@Override
	protected String getName() {
		return "MenuScreen";
	}

}
