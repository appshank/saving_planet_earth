package com.appshank.spe.screens.scene2d;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.appshank.spe.domain.Item;
import com.appshank.spe.domain.Planet;
import com.appshank.spe.domain.PlanetModel;
import com.appshank.spe.domain.Shield;
import com.appshank.spe.screens.GameScreen;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * A planets 2D representation
 * <p>
 * 
 * @author AppShank
 * 
 */
public class Planet2D extends Image {

	/**
	 * The ship's maximum speed; given in pixels per second.
	 */
	public static final float MAX_SPEED = 5000;

	private static final float SHIELD_BLINK_CYCLE_TIME = 0.6f;

	// planet's bottom left position
	private final Vector2 position;

	// temp vector to hold the planet's center position for collision
	// calculation purpose
	private final Vector2 collisionCenter = new Vector2();

	// the planet
	private final Planet planet;

	// Actors for installed items
	private Image shield2d;

	// damage absorption capability in case that we have a shield
	private int currentShieldDamageAbsorptionCapability;

	/**
	 * Creates a new {@link Planet2D}
	 * 
	 * @return
	 */
	public static Planet2D create(TextureAtlas textureAtlas) {
		Planet planet = new Planet();

		// reset planet models
		resetPlanetModelHealths();

		planet.install(PlanetModel.EARTH);
		return create(planet, textureAtlas);
	}

	private static void resetPlanetModelHealths() {
		for (PlanetModel planetModel : PlanetModel.values()) {
			planetModel.resetHealth();
		}
	}

	/**
	 * Factory method to create a {@link Planet2D}
	 * <p>
	 * 
	 * @param planet
	 * @return
	 */
	public static Planet2D create(Planet planet, TextureAtlas textureAtlas) {
		return new Planet2D(planet, textureAtlas);
	}

	private Planet2D(Planet planet, TextureAtlas textureAtlas) {
		super(textureAtlas.findRegion("game-screen/"
				+ planet.getPlanetModel().getSimpleName()));
		this.planet = planet;
		this.position = new Vector2();

		// store the actors for the installable items
		shield2d = new Image(textureAtlas.findRegion("game-screen/"
				+ Shield.MS.getSimpleName()));
	}

	public void setPosition(float x, float y) {
		position.set(x, y);
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		movePlanet(delta);
	}

	/**
	 * Moves the ship around the screen.
	 */
	private void movePlanet(float delta) {
		// check the input and calculate the position
		if (Gdx.app.getType() == ApplicationType.Desktop) {
			// do nothing yet
		} else if (Gdx.app.getType() == ApplicationType.WebGL) {
			// do nothing yet
		} else if (Gdx.app.getType() == ApplicationType.iOS) {
			// do nothing yet
		} else if (Gdx.app.getType() == ApplicationType.Android) {
			// do nothing yet
		}

		// update the planet's actual position
		setX(position.x);
		setY(position.y);

		// update the collisionCenter
		collisionCenter.set(position.x + getWidth() / 2, position.y
				+ getHeight() / 2);

		// update item positions
		if (planet.getShield() != null)
			updateShieldPosition();
	}

	public float getRadius() {
		if (planet.getShield() != null) {
			// return the shield width/2
			return shield2d.getWidth() / 2;
		} else
			return getWidth() / 2;
	}

	/**
	 * check for collision of this planet with the meteor passed as argument
	 * 
	 * @param meteor2d
	 */
	public boolean checkCollision(Meteor2D meteor2d) {
		boolean collision = false;
		// they have collided if the distance between their centers is less than
		// their radius
		if (collisionCenter.dst(meteor2d.getCollisionCenter()) < getRadius()
				+ meteor2d.getRadius() - 6) {
			// if control reaches here that the planet has collided with the
			// meteor
			collision = true;

			// update the planet accordingly
			collided(meteor2d);
		}
		return collision;
	}

	public boolean checkCollision(BonusPack2D bonusPack2D) {
		boolean collision = false;

		if (!bonusPack2D.isActive())
			return collision;

		// they have collided if the distance between their centers is less than
		// their radius
		if (collisionCenter.dst(bonusPack2D.getCollisionCenter()) < getRadius()
				+ bonusPack2D.getRadius() - 4) {
			// if control reaches here that the planet has collided with the
			// meteor
			collision = true;

			// deactivate the bonus pack to mark it as absorbed
			bonusPack2D.setActive(false);

			// update the planet accordingly
			collided(bonusPack2D);
		}
		return collision;
	}

	/**
	 * actions to be taken in case there has been a collision between planet and
	 * meteor
	 * 
	 * @param meteor2d
	 */
	private void collided(Meteor2D meteor2d) {
		// if there is shield on the planet then decrease its health
		int damage;
		if (planet.getShield() != null) {
			damage = decreaseShieldAbsorptionCapability(meteor2d.getMeteor()
					.getDamage());
		} else {
			damage = meteor2d.getMeteor().getDamage();
		}

		// decrease the planet's health
		planet.decreaseHealth(damage);

		if (planet.getHealth() <= 0) {
			GameScreen.setGameOver();
		}
	}

	private int decreaseShieldAbsorptionCapability(int damage) {
		currentShieldDamageAbsorptionCapability -= damage;
		int damageNotAbsorbed = 0;
		if (currentShieldDamageAbsorptionCapability <= 0) {
			damageNotAbsorbed = -currentShieldDamageAbsorptionCapability;
			removeShield();
		}
		return damageNotAbsorbed;
	}

	private void removeShield() {
		// reset currentShieldAbsorptionCapability
		currentShieldDamageAbsorptionCapability = 0;

		// remove the shield2d actor
		getStage().getActors().removeValue(shield2d, true);

		// set shield of planet to null
		planet.removeShield();
	}

	private void collided(BonusPack2D bonusPack2D) {
		// do nothing here
	}

	public Planet getPlanet() {
		return planet;
	}

	/**
	 * install the item to the planet
	 * 
	 * @param item
	 */
	public void install(Item item) {
		// install item
		planet.install(item);

		// add shield2d to stage
		if (item instanceof Shield) {
			getStage().addActor(shield2d);

			shield2d.clearActions();
			shield2d.addAction(getShieldActions());

			// increase the currentShieldDamageAbsorptionCapability
			currentShieldDamageAbsorptionCapability += ((Shield) item)
					.getDamageAbsorptionCapability();

			updateShieldPosition();
		}
	}

	private Action getShieldActions() {
		return forever(sequence(alpha(0.1f, SHIELD_BLINK_CYCLE_TIME / 2),
				alpha(1f, SHIELD_BLINK_CYCLE_TIME / 2)));
	}

	private void updateShieldPosition() {
		float shieldX = collisionCenter.x - shield2d.getWidth() / 2;
		float shieldY = collisionCenter.y - shield2d.getHeight() / 2;
		shield2d.setPosition(shieldX, shieldY);
	}

	public Vector2 getCollisionCenter() {
		return collisionCenter;
	}

	public void install(ShieldPack2D shieldPack2D) {
		install(shieldPack2D.getShieldPack().getShield());
	}

}
