package com.appshank.spe.screens.scene2d;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ParticleEffectActor extends Actor {
	ParticleEffect particleEffect;

	public ParticleEffectActor(ParticleEffect effect) {
		this.particleEffect = effect;
	}

	public void draw(SpriteBatch batch, float parentAlpha) {
		particleEffect.draw(batch); // define behavior when stage calls Actor.draw()
	}

	public void act(float delta) {
		super.act(delta);
		// effect.setPosition(x, y); // set to whatever x/y you prefer
		particleEffect.update(delta); // update it
	}

	public ParticleEffect getEffect() {
		return particleEffect;
	}
}