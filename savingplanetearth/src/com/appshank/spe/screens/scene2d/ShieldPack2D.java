package com.appshank.spe.screens.scene2d;

import com.appshank.spe.domain.ShieldPack;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Scaling;

public class ShieldPack2D extends BonusPack2D {

	// the shield pack for this 2D
	private ShieldPack shieldPack;

	public ShieldPack2D(float centerX, float centerY, Drawable drawable,
			float life) {
		super(centerX, centerY, drawable, Scaling.fit, life);
		this.shieldPack = new ShieldPack();
	}

	public ShieldPack getShieldPack() {
		return shieldPack;
	}

	@Override
	public int getType() {
		return shieldPack.getType();
	}

}
