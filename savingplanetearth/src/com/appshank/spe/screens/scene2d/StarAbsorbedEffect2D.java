package com.appshank.spe.screens.scene2d;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.factories.ParticleEffectFactory;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class StarAbsorbedEffect2D extends ParticleEffectActor {

	public StarAbsorbedEffect2D(float x, float y, ParticleEffect effect) {
		super(effect);

		// update the particle effects position
		particleEffect.setPosition(x, y);
		particleEffect.start();
	}

	public static StarAbsorbedEffect2D create(float x, float y) {
		return new StarAbsorbedEffect2D(x, y,
				ParticleEffectFactory.getStarAbsorptionPE());
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		Gdx.app.log(SavingPlanetEarth.LOG, "drawing star absoption actor at : "
				+ getX() + "," + getY());
		super.draw(batch, parentAlpha);

		Gdx.app.log(SavingPlanetEarth.LOG, "particleEffect.isComplete() : "
				+ particleEffect.isComplete());

		if (particleEffect.isComplete()) {
			getStage().getActors().removeValue(this, true);
		}
	}
}
