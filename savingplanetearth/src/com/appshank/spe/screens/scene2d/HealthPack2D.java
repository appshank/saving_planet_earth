package com.appshank.spe.screens.scene2d;

import com.appshank.spe.domain.HealthPack;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Scaling;

public class HealthPack2D extends BonusPack2D {

	// the type of HealthPack
	private HealthPack healthPack;

	// centerPosition, drawable
	public HealthPack2D(float centerX, float centerY, Drawable drawable,
			float life) {
		super(centerX, centerY, drawable, Scaling.fit, life);
		this.healthPack = new HealthPack();
	}

	public HealthPack getHealthPack() {
		return healthPack;
	}

	@Override
	public int getType() {
		return healthPack.getType();
	}

}
