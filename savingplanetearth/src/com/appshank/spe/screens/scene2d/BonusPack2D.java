package com.appshank.spe.screens.scene2d;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Scaling;

public abstract class BonusPack2D extends Image {

	// the time for which it shall remain active
	float life;
	boolean active;

	// location of the healthPack (center)
	private Vector2 centerPosition;

	public BonusPack2D(float centerX, float centerY, Drawable drawable,
			Scaling scaling, float life) {
		super(drawable, scaling);
		this.centerPosition = new Vector2(centerX, centerY);
		setPosition(centerX - getWidth() / 2, centerY - getHeight() / 2);
		this.life = life;
		this.setActive(true);
	}

	public abstract int getType();

	public void update(float delta) {
		// only decrease the life in case the bonus pack is still active i.e. it
		// hasnt been absorbed
		if (isActive())
			life -= delta;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}

	public boolean isAlive() {
		if (life <= 0)
			return false;
		else
			return true;
	}

	public Vector2 getCollisionCenter() {
		return centerPosition;
	}

	public float getRadius() {
		return getWidth() / 2;
	}
}
