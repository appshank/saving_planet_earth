package com.appshank.spe.screens.scene2d;

import com.appshank.spe.domain.StarPack;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Scaling;

public class StarPack2D extends BonusPack2D {

	// the type of starPack
	private StarPack starPack;

	// centerPosition, drawable
	public StarPack2D(float centerX, float centerY, Drawable drawable,
			float life) {
		super(centerX, centerY, drawable, Scaling.fit, life);
		this.starPack = new StarPack();
	}

	public StarPack getStarPack() {
		return starPack;
	}

	@Override
	public int getType() {
		return starPack.getType();
	}

}
