package com.appshank.spe.screens.scene2d;

import java.util.Random;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.Meteor;
import com.appshank.spe.domain.factories.ParticleEffectFactory;
import com.appshank.spe.utils.Corner;
import com.appshank.spe.utils.MeteorSpeedCalculator;
import com.appshank.spe.utils.VectorUtils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Meteor2D extends ParticleEffectActor {

	// static random instance
	private static Random random;

	// the width and height of the stage
	private static float stageWidth, stageHeight;

	// the type of the meteor
	private Meteor meteor;

	// the collision center for the meteor
	// we are using this instead of the position as this lags behind the
	// position by one frame and hence provides a more realistic collision
	// center
	private Vector2 collisionCenter;

	// location of the meteor (center)
	private Vector2 position;

	// the default initial speed of the meteor
	private static float MIN_INITIAL_SPEED;
	private static float MAX_INITIAL_SPEED;
	private static float INCREMENT;
	private static float SMALL_INCREMENT; // after MAX_SPEED_KNEE1
	private static float MINI_INCREMENT; // after MAX_SPEED_KNEE2
	private static float MICRO_INCREMENT; // after MAX_SPEED_KNEE3
	private static float MAX_SPEED_KNEE1;
	private static float MAX_SPEED_KNEE2;
	private static float MAX_SPEED_KNEE3;
	private static float MAX_SPEED_ABS;

	// speed limit increment per thousand score
	private static float INCREMENT_PER_THOUSAND;

	// the current speed of meteor
	private float speed;

	// the current velocity of meteor
	private Vector2 velocity;
	private float angleDegrees;
	private float cos, sin;

	// flag to check if this is a new meteor which is currently entering the
	// space
	private boolean isEntering = true;

	// random meteor constructor (corner)
	// random type
	// random angle as per corner (10,80)
	public static Meteor2D create(Corner corner, float stageWidth,
			float stageHeight) {
		if (random == null)
			random = new Random();

		// random meteor (regular 70% and huge 30%)
		Meteor meteor = (random.nextFloat() < 0.7) ? Meteor.REGULAR
				: Meteor.HUGE;

		// random angle
		float angleDegrees = randomAngleByCorner(corner);

		ParticleEffect effect = ParticleEffectFactory.getMeteorPE();

		return new Meteor2D(meteor, corner, stageWidth, stageHeight,
				angleDegrees, effect);
	}

	/**
	 * creates a random angle as per corner
	 * 
	 * @param corner
	 * @return
	 */
	private static float randomAngleByCorner(Corner corner) {
		// settings for (30,60)
		float padding = 30;

		float minAngleDegrees = 0;
		float maxAngleDegrees = 90;
		switch (corner) {
		case BOTTOM_LEFT:
			minAngleDegrees = padding;
			maxAngleDegrees = 90 - padding;
			break;
		case BOTTOM_RIGHT:
			minAngleDegrees = 90 + padding;
			maxAngleDegrees = 180 - padding;
			break;
		case TOP_RIGHT:
			minAngleDegrees = 180 + padding;
			maxAngleDegrees = 270 - padding;
			break;
		case TOP_LEFT:
			minAngleDegrees = 270 + padding;
			maxAngleDegrees = 360 - padding;
			break;
		}

		return MathUtils.random(minAngleDegrees, maxAngleDegrees);
	}

	// constructor (meteor type, corner, stageWidth, stageHeight, angleDegrees)
	private Meteor2D(Meteor meteor, Corner corner, float stageWidth,
			float stageHeight, float angleDegrees, ParticleEffect effect) {
		super(effect);

		particleEffect.start();

		if (random == null)
			random = new Random();

		this.meteor = meteor;
		this.speed = MathUtils.random(MIN_INITIAL_SPEED, MAX_INITIAL_SPEED);
		Meteor2D.stageWidth = stageWidth;
		Meteor2D.stageHeight = stageHeight;
		this.position = new Vector2();
		this.angleDegrees = angleDegrees;
		this.cos = MathUtils.cosDeg(angleDegrees);
		this.sin = MathUtils.sinDeg(angleDegrees);
		this.collisionCenter = new Vector2();

		// radian angle
		float angleRadian = (float) Math.toRadians(angleDegrees);

		// use the corner to determine the initial position
		setInitialPosition(position, corner, stageWidth, stageHeight,
				angleDegrees);

		// initial velocity as per angle
		velocity = new Vector2((float) (speed * Math.cos(angleRadian)),
				(float) (speed * Math.sin(angleRadian)));

		// set the flag for isEntering
		isEntering = true;

	}

	private void setInitialPosition(Vector2 position, Corner corner,
			float stageWidth, float stageHeight, float angleDegrees) {

		// the initial position should be such that the meteor enters the screen
		// at a corner
		// i.e. the initial position is obtained by interpolation the corner
		// with the movement angle

		// corner x and y
		float cx = cxOfCorner(corner, stageWidth);
		float cy = cyOfCorner(corner, stageHeight);

		// tangent of angleDegrees
		float tanOfAngle = (float) Math.tan(Math.toRadians(angleDegrees));

		// modAngleDegrees
		float modAngleDegrees = angleDegrees % 90f;

		// calculate x
		position.x = cx;
		float doubleMeteorDia = 2 * meteor.getDia();
		switch (corner) {
		case BOTTOM_LEFT:
			if (modAngleDegrees < 45)
				position.x -= doubleMeteorDia;
			else
				position.x -= doubleMeteorDia / tanOfAngle;
			break;
		case BOTTOM_RIGHT:
			if (modAngleDegrees < 45)
				position.x -= doubleMeteorDia / tanOfAngle;
			else
				position.x += doubleMeteorDia;
			break;
		case TOP_RIGHT:
			if (modAngleDegrees < 45)
				position.x += doubleMeteorDia;
			else
				position.x += doubleMeteorDia / tanOfAngle;
			break;
		case TOP_LEFT:
			if (modAngleDegrees < 45)
				position.x += doubleMeteorDia / tanOfAngle;
			else
				position.x -= doubleMeteorDia;
			break;
		}

		// y-py = (x-px)tanAngle
		position.y = cy + (position.x - cx) * tanOfAngle;

	}

	private float cxOfCorner(Corner corner, float stageWidth) {
		float cx = 0;
		switch (corner) {
		case BOTTOM_LEFT:
			cx = 0;
			break;
		case BOTTOM_RIGHT:
			cx = stageWidth;
			break;
		case TOP_RIGHT:
			cx = stageWidth;
			break;
		case TOP_LEFT:
			cx = 0;
			break;
		}
		return cx;
	}

	private float cyOfCorner(Corner corner, float stageHeight) {
		float cy = 0;
		switch (corner) {
		case BOTTOM_LEFT:
			cy = 0;
			break;
		case BOTTOM_RIGHT:
			cy = 0;
			break;
		case TOP_RIGHT:
			cy = stageHeight;
			break;
		case TOP_LEFT:
			cy = stageHeight;
			break;
		}
		return cy;
	}

	// act
	@Override
	public void act(float delta) {
		super.act(delta);
		increaseSpeed(delta);
		move(delta);
	}

	private void increaseSpeed(float delta) {
		if (speed < MAX_SPEED_KNEE1) {
			speed += INCREMENT * delta;
			// Gdx.app.log(SavingPlanetEarth.LOG, "speed : " + speed);
			velocity.add(INCREMENT * cos, INCREMENT * sin);
		} else if (speed < MAX_SPEED_KNEE2) {
			speed += SMALL_INCREMENT * delta;
			// Gdx.app.log(SavingPlanetEarth.LOG, "speed : " + speed);
			velocity.add(SMALL_INCREMENT * cos, SMALL_INCREMENT * sin);
		} else if (speed < MAX_SPEED_KNEE3) {
			speed += MINI_INCREMENT * delta;
			// Gdx.app.log(SavingPlanetEarth.LOG, "speed : " + speed);
			velocity.add(MINI_INCREMENT * cos, MINI_INCREMENT * sin);
		} else if (speed < MAX_SPEED_ABS) {
			speed += MICRO_INCREMENT * delta;
			// Gdx.app.log(SavingPlanetEarth.LOG, "speed : " + speed);
			velocity.add(MICRO_INCREMENT * cos, MICRO_INCREMENT * sin);
		}
	}

	// move and update position
	private void move(float delta) {

		// tmp
		boolean isDebug = false;
		if (isDebug) {
			// position.x = 0;
			// position.y = 0;

			// update the particle effects position
			// this method sets the position to be the center of the particle
			// effect (this is the point where the emitters are based)
			particleEffect.setPosition(position.x--, position.y--);

			setX(position.x - getWidth() / 2);
			setY(position.y - getHeight() / 2);

			return;
		}

		// tmp
		collisionCenter.set(position);

		// move according to velocity
		// update the position variable
		position.x += velocity.x * delta;
		position.y += velocity.y * delta;

		if (!isEntering) {
			// check that the new position is in bounds
			// revert the x and y speeds in case it goes out of bounds
			boolean rebound = false;
			if (VectorUtils.adjustByRangeX(position, 0, stageWidth)) {
				// if control reaches here that means that position was modified
				// and
				// we need to reverse the direction of velocity along x
				velocity.x = -velocity.x;
				rebound = true;
			}
			if (VectorUtils.adjustByRangeY(position, 0, stageHeight)) {
				// if control reaches here that means that position was modified
				// and
				// we need to reverse the direction of velocity along y
				velocity.y = -velocity.y;
				rebound = true;
			}

			if (rebound) {
				// update angleDegrees, cos and sin
				angleDegrees = velocity.angle();
				cos = MathUtils.cosDeg(angleDegrees);
				sin = MathUtils.sinDeg(angleDegrees);
			}

		} else {
			if (VectorUtils.isInRange(position, getRadius(), stageWidth
					- getRadius(), getRadius(), stageHeight - getRadius()))
				isEntering = false;
		}

		// set x and y of the actor as per the position variable
		// currently the width and height are both zero as we are directly
		// extending actor and not setting the width and height of the actor
		setX(position.x - getWidth() / 2);
		setY(position.y - getHeight() / 2);

		// update the particle effects position
		particleEffect.setPosition(position.x, position.y);
	}

	// override the draw method to draw the particle effect for the meteor
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
	}

	// getters
	public Vector2 getPosition() {
		return position;
	}

	public float getRadius() {
		return meteor.getDia() / 2;
	}

	public Vector2 getCollisionCenter() {
		return collisionCenter;
	}

	public Meteor getMeteor() {
		return meteor;
	}

	/**
	 * call this method to destroy the meteor
	 */
	public void destroy() {
		// TODO Auto-generated method stub
		particleEffect.dispose();
	}

	public static void increaseLimits() {
		Gdx.app.log(SavingPlanetEarth.LOG, "speed limits increased !");
		MIN_INITIAL_SPEED += INCREMENT_PER_THOUSAND;
		MAX_INITIAL_SPEED += INCREMENT_PER_THOUSAND;
		MAX_SPEED_KNEE1 += INCREMENT_PER_THOUSAND;
		MAX_SPEED_KNEE2 += INCREMENT_PER_THOUSAND;
		MAX_SPEED_KNEE3 += INCREMENT_PER_THOUSAND;
		MAX_SPEED_ABS += INCREMENT_PER_THOUSAND;
	}

	public static void initSpeeds(MeteorSpeedCalculator speedCalculator) {
		MIN_INITIAL_SPEED = speedCalculator.getMinInitialSpeed();
		MAX_INITIAL_SPEED = speedCalculator.getMaxInitialSpeed();
		INCREMENT = speedCalculator.getIncrement();
		SMALL_INCREMENT = speedCalculator.getSmallIncrement(); // after
																// MAX_SPEED_KNEE1
		MINI_INCREMENT = speedCalculator.getMiniIncrement(); // after
																// MAX_SPEED_KNEE2
		MICRO_INCREMENT = speedCalculator.getMicroIncrement(); // after
																// MAX_SPEED_KNEE3
		MAX_SPEED_KNEE1 = speedCalculator.getMaxSpeedKnee1();
		MAX_SPEED_KNEE2 = speedCalculator.getMaxSpeedKnee2();
		MAX_SPEED_KNEE3 = speedCalculator.getMaxSpeedKnee3();
		MAX_SPEED_ABS = speedCalculator.getMaxSpeedAbs();

		// speed limit increment per thousand score
		INCREMENT_PER_THOUSAND = speedCalculator.getIncrementPerThousand();
	}

}
