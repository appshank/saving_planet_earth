package com.appshank.spe.screens.scene2d;

import com.appshank.spe.SavingPlanetEarth;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class PlanetExplosion2D extends ParticleEffectActor {
	public PlanetExplosion2D(float x, float y, ParticleEffect effect) {
		super(effect);

		// update the particle effects position
		particleEffect.setPosition(x, y);
		particleEffect.start();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		Gdx.app.log(SavingPlanetEarth.LOG, "drawing explosion actor at : "
				+ getX() + "," + getY());
		super.draw(batch, parentAlpha);

		Gdx.app.log(SavingPlanetEarth.LOG, "Explosion particleEffect.isComplete() : "
				+ particleEffect.isComplete());

		if (particleEffect.isComplete()) {
			getStage().getActors().removeValue(this, true);
		}
	}

	public boolean isParticleEffectComplete() {
		return particleEffect.isComplete();
	}
}
