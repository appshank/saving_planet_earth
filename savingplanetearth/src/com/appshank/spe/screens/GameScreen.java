package com.appshank.spe.screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.repeat;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.rotateBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.BonusPack;
import com.appshank.spe.domain.Profile;
import com.appshank.spe.domain.factories.BonusPackFactory;
import com.appshank.spe.domain.factories.FontFactory;
import com.appshank.spe.domain.factories.MeteorFactory;
import com.appshank.spe.domain.factories.ParticleEffectFactory;
import com.appshank.spe.screens.scene2d.BonusPack2D;
import com.appshank.spe.screens.scene2d.HealthPack2D;
import com.appshank.spe.screens.scene2d.Meteor2D;
import com.appshank.spe.screens.scene2d.MeteorCollision2D;
import com.appshank.spe.screens.scene2d.Planet2D;
import com.appshank.spe.screens.scene2d.PlanetExplosion2D;
import com.appshank.spe.screens.scene2d.ShieldPack2D;
import com.appshank.spe.screens.scene2d.StarAbsorbedEffect2D;
import com.appshank.spe.screens.scene2d.StarPack2D;
import com.appshank.spe.services.MusicManager.SPEMusic;
import com.appshank.spe.services.SoundManager.SPESound;
import com.appshank.spe.utils.Corner;
import com.appshank.spe.utils.VectorUtils;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;

public class GameScreen extends AbstractScreen {

	public enum GameState {
		STARTING, RUNNING, PAUSED, GAMEOVER, RESUMING;
	}

	private static final float FINAL_CORNER_ALPHA = 0.5f;
	private static final float FINAL_COLLISION_NOTIFICATION_IMAGE_ALPHA = 0.35f;
	private static final float FINAL_COLLISION_NOTIFICATION_IMAGE_ALPHA_TIME = 0.1f;
	private static final float METEOR_CREATION_TIME = 2f;
	private static final float FINAL_CORNER_ALPHA_TIME = (METEOR_CREATION_TIME - 3 * FINAL_COLLISION_NOTIFICATION_IMAGE_ALPHA_TIME) / 4 / 2;
	private static final float BONUS_PACK_ABSORPTION_ANIMATION_TIME = 1f;
	private static final float BONUS_PACK_ABSORPTION_ANIMATION_SCALE = 2f;
	private static final float BONUS_PACK_EXPANSION_ANIMATION_TIME = 0.4f;
	private static final float BONUS_PACK_EXPANSION_ANIMATION_SCALE = 6f;
	private static final float COUNTER_TIME = 3f;

	private Profile profile;
	private Planet2D planet2d;
	private PlanetExplosion2D planetExplosion2D = null;

	// click location refers to the exact point of click on the screen
	private Vector2 clickLocation;
	// planet location refers to the bottom left of the planet's location
	private Vector2 planetLocation;
	// target planetlocation refers to the bottom left of the planet's target
	// location
	private Vector2 targetPlanetLocation;

	public static Array<Meteor2D> meteors2d;

	// relating to the texts displayed on the screen
	LabelStyle labelStyle;
	LabelStyle counterStyle;

	Label healthLabel;
	Label scoreLabel;
	Label pauseLabel;
	Label counterLabel;

	Image collisionIndicationImage;
	Array<Image> cornerImages;
	Array<Corner> meteorEnteringCorners;
	Array<Corner> oldMeteorEnteringCorners;

	public static Array<BonusPack2D> activeBonusPacks;

	public static GameState gameState;
	public boolean gameOverFirst = true;

	// tmp stage actors
	Label fpsLabel;
	StringBuilder sb = new StringBuilder();

	// items to be displayed when game is over
	Image gameOverBGImage = null;
	private Table table;
	Label gameOverLabel = null;
	Label finalScoreLabel = null;
	Label newHighScoreLabel = null;
	Label creditsEarnedLabel = null;
	TextButton tryAgainButton = null;
	TextButton goToMainMenuButton = null;

	private static final float SCORE_LABEL_CENTER_X = 110f;
	private static final float SCORE_LABEL_CENTER_Y = 20f;
	private static final float HEALTH_LABEL_CENTER_X = 130f;

	private int previousThousand = 0;

	float starActorFinalX, starActorFinalY;
	float healthActorFinalX, healthActorFinalY;

	// tmp
	Image scoreLabelCenterHorizontalLine;
	Image scoreLabelCenterVerticalLine;
	Image healthLabelCenterVerticalLine;

	public GameScreen(SavingPlanetEarth game) {
		super(game);

		// reset game stats
		game.gameStats.reset();

		// get the profile
		profile = game.getProfileManager().retrieveProfile();
	}

	@Override
	public void show() {
		super.show();

		// play the level music
		game.getMusicManager().play(SPEMusic.LEVEL);

		// set the background
		Image backgroundImage = new Image(new TextureRegionDrawable(getAtlas()
				.findRegion("game-screen/background-2")), Scaling.fill);
		backgroundImage.setTouchable(Touchable.disabled);
		stage.addActor(backgroundImage);
		backgroundImage.setFillParent(true);

		// add the touchlistener to the stage
		stage.addListener(new GameTouchListener());

		meteors2d = new Array<Meteor2D>();
		meteorEnteringCorners = new Array<Corner>();
		oldMeteorEnteringCorners = new Array<Corner>();
		cornerImages = new Array<Image>(4);
		activeBonusPacks = new Array<BonusPack2D>();
	}

	@Override
	public void render(float delta) {
		if (delta > 0.03) {
			delta = 0.03f;
		}

		// super.render(delta);

		if (gameState == GameState.GAMEOVER) {

			if (gameOverFirst) {
				// remove earth from display
				planet2d.setVisible(false);
			}

			// update the planet explosion actor
			Array<Actor> actors = stage.getActors();
			for (Actor actor : actors) {
				if (actor instanceof MeteorCollision2D)
					actor.act(delta);
			}
			planetExplosion2D.act(delta);

			if (gameOverFirst && planetExplosion2D.isParticleEffectComplete()) {
				gameOverFirst = !gameOverFirst;
				doStuffAtGameOver();
			}

			// display the overlay background

			// display the number of credits earned

			// display button to restart game

			// draw the stage
			stage.draw();
		}

		if (gameState == GameState.PAUSED) {
			// do nothing

			// draw the stage
			stage.draw();
		}

		if (gameState == GameState.RESUMING) {
			// call act for only the counterLabel actor
			if (counterLabel != null)
				counterLabel.act(delta);

			// draw the stage
			stage.draw();
		}

		if (gameState == GameState.RUNNING) {
			game.gameStats.gameDuration += delta;

			super.render(delta);

			// check collisions
			checkCollisions(delta);

			// update meteorfactory
			MeteorFactory.update(delta);

			// update bonusPackFactory
			BonusPackFactory.update(delta);

			// update meteorEnteringCorners
			updateMeteorEnteringCorners();

			// remove expired bonus packs
			removeExpiredBonusPacks(delta);

			// score
			updateScore(delta);

			// update the labels
			updateLabelTexts();
		}

	}

	private void updateScore(float delta) {
		if (!(gameState == GameState.GAMEOVER)) {
			game.gameStats.score += delta * 10f;

			if ((int) (game.gameStats.score / 1000) > previousThousand) {
				previousThousand = (int) (game.gameStats.score / 1000);
				Meteor2D.increaseLimits();
			}
		}
	}

	private void doStuffAtGameOver() {
		// highscore
		boolean isNewHighScore = updateHighscores();

		// update credits
		int creditsEarned = calculateCreditsEarned();
		Gdx.app.log(SavingPlanetEarth.LOG, "credits earned in this game : "
				+ creditsEarned);
		profile.addCredits(creditsEarned);

		// initialize the game over actors after the planet explosion is
		// complete
		if (planetExplosion2D != null
				&& planetExplosion2D.isParticleEffectComplete()) {
			// initialize the gameOverActors in case they are null
			initializeGameOverActors(isNewHighScore);
		}

		// update the gameStats
		Gdx.app.log(SavingPlanetEarth.LOG, game.gameStats.toString());

		// update the profile stats
		profile.updateProfileStats(game.gameStats);

	}

	private void removeExpiredBonusPacks(float delta) {
		for (BonusPack2D bonusPack2D : activeBonusPacks) {
			bonusPack2D.update(delta);
			if (!bonusPack2D.isAlive()) {
				// remove the pack from array and stage
				activeBonusPacks.removeValue(bonusPack2D, true);
				stage.getActors().removeValue(bonusPack2D, true);
			}
		}
	}

	private Action getCornerActions() {
		return sequence(
				delay(0.1f),
				repeat(4,
						sequence(
								alpha(FINAL_CORNER_ALPHA,
										FINAL_CORNER_ALPHA_TIME),
								alpha(0f, FINAL_CORNER_ALPHA_TIME))));
	}

	private void updateMeteorEnteringCorners() {

		// update the corners array
		meteorEnteringCorners = MeteorFactory.getCorners();

		if (!oldMeteorEnteringCorners.equals(meteorEnteringCorners)) {
			Gdx.app.log(SavingPlanetEarth.LOG, "inside setup of corners");

			oldMeteorEnteringCorners.clear();
			oldMeteorEnteringCorners.addAll(meteorEnteringCorners);

			// for each corner display the corner image
			if (meteorEnteringCorners.contains(Corner.BOTTOM_LEFT, false)) {
				// bottom left
				if (cornerImages.get(0).getActions().size < 4) {
					cornerImages.get(0).addAction(getCornerActions());
				}

			} else {
				// hide bottom left
				cornerImages.get(0).getColor().a = 0f;
			}
			if (meteorEnteringCorners.contains(Corner.BOTTOM_RIGHT, false)) {
				// bottom right
				if (cornerImages.get(1).getActions().size < 4) {
					cornerImages.get(1).addAction(getCornerActions());
				}
			} else {
				// hide bottom right
				cornerImages.get(1).getColor().a = 0f;
			}
			if (meteorEnteringCorners.contains(Corner.TOP_RIGHT, false)) {
				// bottom left
				if (cornerImages.get(2).getActions().size < 4) {
					cornerImages.get(2).addAction(getCornerActions());
				}
			} else {
				// hide bottom left
				cornerImages.get(2).getColor().a = 0f;
			}
			if (meteorEnteringCorners.contains(Corner.TOP_LEFT, false)) {
				// bottom left
				if (cornerImages.get(3).getActions().size < 4) {
					cornerImages.get(3).addAction(getCornerActions());
				}
			} else {
				// hide bottom left
				cornerImages.get(3).getColor().a = 0f;
			}
		}

	}

	/**
	 * checks collision of planet with the meteors and updates the states of the
	 * actors accordingly
	 */
	private void checkCollisions(float delta) {
		// check for collision of the planet with each meteor
		checkMeteorCollisions(delta);

		// check for collision of planet with each bonus pack
		checkBonusPackCollisions(delta);

	}

	private void checkBonusPackCollisions(float delta) {
		for (BonusPack2D bonusPack2D : activeBonusPacks) {
			// check for collision of planet with this bonusPack
			if (planet2d.checkCollision(bonusPack2D)) {
				// if control reaches here then the planet has collided with
				// the bonusPack
				if (bonusPack2D.getType() == BonusPack.HEALTH) {
					// in case of health, increase the health
					increasePlanetHealth(bonusPack2D);
				} else if (bonusPack2D.getType() == BonusPack.STAR) {
					// increase the score
					increaseScore(bonusPack2D);
				} else if (bonusPack2D.getType() == BonusPack.SHIELD) {
					// add shield to the planet
					planet2d.install((ShieldPack2D) bonusPack2D);
				}

				// bonus pack absorption animation
				bonusPack2D.clearActions();
				bonusPack2D
						.addAction(getBonusPackAbsorptionAction(bonusPack2D));

				// update stats
				game.gameStats.addBonusPackAbsorbed(bonusPack2D.getType());
			}
		}
	}

	private void increaseScore(BonusPack2D bonusPack2D) {
		game.gameStats.score += ((StarPack2D) bonusPack2D).getStarPack()
				.getScoreBonus();
	}

	private void increasePlanetHealth(BonusPack2D bonusPack2D) {
		planet2d.getPlanet().decreaseHealth(
				-((HealthPack2D) bonusPack2D).getHealthPack().getHealthBonus());

	}

	class AbsorptionActorCreatorAction extends Action {
		float finalX, finalY;

		public AbsorptionActorCreatorAction(float finalX, float finalY) {
			this.finalX = finalX;
			this.finalY = finalY;
			Gdx.app.log(SavingPlanetEarth.LOG,
					"creating AbsorptionActorCreatorAction with finalX : "
							+ finalX + " and finalY : " + finalY);
		}

		@Override
		public boolean act(float delta) {
			Gdx.app.log(SavingPlanetEarth.LOG, "finalX = " + finalX
					+ " finalY =" + finalY);

			stage.addActor(StarAbsorbedEffect2D.create(finalX, finalY));
			return true;
		}

	}

	private Action createAndAddAbsorbptionActor(final BonusPack2D bonusPack2D,
			float finalX, float finalY) {
		Gdx.app.log(SavingPlanetEarth.LOG,
				"inside createAndAddAbsorptionActor()");
		return new AbsorptionActorCreatorAction(finalX, finalY);
	}

	private Action removeBonusPackAction(final BonusPack2D bonusPack2D) {
		return new Action() {

			@Override
			public boolean act(float delta) {
				Gdx.app.log(SavingPlanetEarth.LOG,
						"inside removeBonusPackAction()");
				stage.getActors().removeValue(bonusPack2D, true);
				activeBonusPacks.removeValue(bonusPack2D, true);
				return true;
			}
		};
	}

	private Action getBonusPackAbsorptionAction(BonusPack2D bonusPack2D) {

		// set the origin of the bonus pack image to its center for rotation and
		// scaling calculations
		bonusPack2D.setOrigin(bonusPack2D.getWidth() / 2,
				bonusPack2D.getHeight() / 2);

		// calculate the final position of the bonus pack actor
		float finalX = 0f, finalY = 0f;
		if (bonusPack2D.getType() == BonusPack.HEALTH) {
			finalX = healthActorFinalX;
			finalY = healthActorFinalY;
		} else if (bonusPack2D.getType() == BonusPack.STAR) {
			finalX = starActorFinalX;
			finalY = starActorFinalY;
		} else if (bonusPack2D.getType() == BonusPack.SHIELD) {
			// scale the bonus pack to 6 times in short time
			return sequence(
					alpha(BonusPackFactory.FINAL_BONUS_PACK_ALPHA),
					parallel(
							scaleTo(BONUS_PACK_EXPANSION_ANIMATION_SCALE,
									BONUS_PACK_EXPANSION_ANIMATION_SCALE,
									BONUS_PACK_EXPANSION_ANIMATION_TIME),
							alpha(0f, BONUS_PACK_EXPANSION_ANIMATION_TIME)),
					removeBonusPackAction(bonusPack2D));
		}

		Gdx.app.log(SavingPlanetEarth.LOG, "finalX : " + finalX + " finalY : "
				+ finalY);

		// the effect
		// scale to 1.5 times in a short time
		// move to finalX,Y and rotate 360deg and scale to 0 (optional alpha to
		// 0)
		// remove BonusPack

		return sequence(
				alpha(BonusPackFactory.FINAL_BONUS_PACK_ALPHA),
				parallel(scaleTo(BONUS_PACK_ABSORPTION_ANIMATION_SCALE,
						BONUS_PACK_ABSORPTION_ANIMATION_SCALE,
						BONUS_PACK_ABSORPTION_ANIMATION_TIME / 6)),
				parallel(
						scaleTo(0.1f, 0.1f,
								BONUS_PACK_ABSORPTION_ANIMATION_TIME * 5 / 6),
						moveTo(finalX - bonusPack2D.getWidth() / 2, finalY
								- bonusPack2D.getHeight() / 2,
								BONUS_PACK_ABSORPTION_ANIMATION_TIME * 5 / 6),
						rotateBy(360f,
								BONUS_PACK_ABSORPTION_ANIMATION_TIME * 5 / 6)),
				createAndAddAbsorbptionActor(bonusPack2D, finalX, finalY),
				removeBonusPackAction(bonusPack2D));
	}

	private void checkMeteorCollisions(float delta) {
		for (Meteor2D meteor2d : meteors2d) {
			// check for collision of planet with this meteor
			if (planet2d.checkCollision(meteor2d)) {
				// if control reaches here then the planet has collided with the
				// meteor

				// cache the position of this meteor
				float x = meteor2d.getX();
				float y = meteor2d.getY();

				// destroy this meteor
				destroyMeteor(meteor2d);

				// queue new meteor for creation
				MeteorFactory.enqueMeteorCreation(METEOR_CREATION_TIME);

				// collision image indication on screen
				createAndAddCollisionImageIndication();

				// vibrate the phone
				if (Gdx.app.getType() == ApplicationType.Android
						|| Gdx.app.getType() == ApplicationType.iOS)
					vibratePhone();

				// draw the collision particle effect
				createAndAddCollisionActor(x, y);

				// draw the planet explosion effect in case the planet explodes
				if (planet2d.getPlanet().getHealth() <= 0) {
					float planetCenterX = planet2d.getCollisionCenter().x;
					float planetCenterY = planet2d.getCollisionCenter().y;
					createAndAddPlanetExplosionActor(planetCenterX,
							planetCenterY);
				}

			}
		}
	}

	private void createAndAddCollisionImageIndication() {
		if (collisionIndicationImage == null)
			initializeCollisionIndiactionImage();
		collisionIndicationImage.clearActions();
		collisionIndicationImage
				.addAction(repeat(
						1,
						sequence(
								alpha(FINAL_COLLISION_NOTIFICATION_IMAGE_ALPHA,
										FINAL_COLLISION_NOTIFICATION_IMAGE_ALPHA_TIME),
								delay(FINAL_COLLISION_NOTIFICATION_IMAGE_ALPHA_TIME),
								alpha(0f,
										FINAL_COLLISION_NOTIFICATION_IMAGE_ALPHA_TIME),
								delay(FINAL_COLLISION_NOTIFICATION_IMAGE_ALPHA_TIME))));

	}

	private void vibratePhone() {
		Gdx.app.log(SavingPlanetEarth.LOG, "vibrationEnabled : "
				+ game.getPreferencesManager().isVibrationEnabled());
		if (game.getPreferencesManager().isVibrationEnabled())
			Gdx.input.vibrate(500);

	}

	private void destroyMeteor(Meteor2D meteor2d) {
		meteors2d.removeValue(meteor2d, true);
		stage.getActors().removeValue(meteor2d, false);
		Gdx.app.log(SavingPlanetEarth.LOG, "Collision");
		meteor2d.destroy();
	}

	private void initializeCollisionIndiactionImage() {
		// collisionIndicationImage
		collisionIndicationImage = new Image(new TextureRegionDrawable(
				getAtlas().findRegion("game-screen/red")), Scaling.stretch);
		stage.addActor(collisionIndicationImage);
		collisionIndicationImage.getColor().a = 0f;
	}

	private void createAndAddPlanetExplosionActor(float x, float y) {
		// create the particle effect
		Gdx.app.log(SavingPlanetEarth.LOG, "creating planet explosion effect");

		ParticleEffect effect = new ParticleEffect();
		effect.load(Gdx.files.internal("particle-effects/meteor_collision.p"),
				Gdx.files.internal("particle-effects"));

		// create the collision effect actor
		planetExplosion2D = new PlanetExplosion2D(x, y, effect);

		// add this actor to the stage
		stage.addActor(planetExplosion2D);
	}

	private void updateLabelTexts() {
		fpsLabel.setText("FPS: " + Gdx.graphics.getFramesPerSecond());
		healthLabel.setText("HEALTH : " + planet2d.getPlanet().getHealth());
		// Gdx.app.log(SavingPlanetEarth.LOG, "score : " +
		// game.gameStats.score);
		scoreLabel.setText("SCORE: " + (int) game.gameStats.score);
	}

	/**
	 * Listener for touch on screen so that planet can be moved according to the
	 * touch
	 */
	private class GameTouchListener extends ClickListener {
		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			super.touchDown(event, x, y, pointer, button);

			Gdx.app.log(SavingPlanetEarth.LOG,
					"gameTouchListener(), inputEvent, touchDown at " + x + ","
							+ y);

			Gdx.app.log(SavingPlanetEarth.LOG, "inputEvent listener actor : "
					+ event.getListenerActor());

			// in case the touchdown in not at pause, move the planet to that
			// position
			if (pauseLabel.hit(x - pauseLabel.getX(), y - pauseLabel.getY(),
					true) == null) {
				Gdx.app.log(SavingPlanetEarth.LOG,
						"pauseLabel.hit returned null");
				if (gameState == GameState.RUNNING)
					commandPlanetTo(x, y);
			} else {
				Gdx.app.log(
						SavingPlanetEarth.LOG,
						"pauseLabel.hit returned : "
								+ pauseLabel.hit(x - pauseLabel.getX(), y
										- pauseLabel.getY(), true));

				// if the control reaches here that means that the touch on
				// pause label has been detected.
				// pause the game
				Gdx.app.log(SavingPlanetEarth.LOG, "Pause label clicked");

				Gdx.app.log(SavingPlanetEarth.LOG, "previous gameState : "
						+ gameState.name());

				if (gameState == GameState.RUNNING) {
					gameState = GameState.PAUSED;
				} else if (gameState == GameState.PAUSED) {
					gameResumeCourse();
				}

				Gdx.app.log(SavingPlanetEarth.LOG, "gameState changed to : "
						+ gameState.name());
			}

			return true;
		}

		@Override
		public void touchDragged(InputEvent event, float x, float y, int pointer) {
			super.touchDragged(event, x, y, pointer);

			commandPlanetTo(x, y);
		}

	}

	private void commandPlanetTo(float x, float y) {

		// Gdx.app.log(SavingPlanetEarth.LOG, "inside commandPlanet()");

		// clear the actions on planet
		planet2d.clearActions();

		if (clickLocation == null)
			clickLocation = new Vector2();
		clickLocation.set(x, y);

		// set the target location to be the click location
		if (targetPlanetLocation == null)
			targetPlanetLocation = new Vector2();
		targetPlanetLocation.set(clickLocation.x - planet2d.getWidth() / 2,
				clickLocation.y - planet2d.getHeight() / 2);

		// add move action to the planet for the new location
		VectorUtils.adjustByRange(targetPlanetLocation, 0, stage.getWidth()
				- planet2d.getWidth(), 0,
				stage.getHeight() - planet2d.getHeight());

		// set the planet location
		if (planetLocation == null)
			planetLocation = new Vector2();
		planetLocation.set(planet2d.getX(), planet2d.getY());

		// subtract the planet location from the click location to calculate
		// the distance to be traveled
		clickLocation.sub(planetLocation);

		float timeToTravel = clickLocation.len() / Planet2D.MAX_SPEED;
		// Gdx.app.log(SavingPlanetEarth.LOG, "timeToTravel: " +
		// timeToTravel);

		planet2d.addAction(moveTo(targetPlanetLocation.x,
				targetPlanetLocation.y, timeToTravel));

	}

	private void gameResumeCourse() {
		// change the gameState to resuming
		gameState = GameState.RESUMING;
		// create the 3-2-1 countdown and then resume the game
		if (counterLabel == null)
			initializeCounterLabel(stage.getWidth(), stage.getHeight());

		counterLabel.clearActions();
		counterLabel.addAction(getCounterActions());
	}

	private Action getCounterActions() {
		// reset scale
		// 3 for COUNTER_TIME/3/2
		// scale and fadeout
		// reset scale
		// 2 for COUNTER_TIME/3/2
		// scale and fadeout
		// reset scale
		// 1 for COUNTER_TIME/3/2
		// scale and fadeout
		// change gameState to running;
		return sequence(getSetCounterTextAction("3"),
				getCounterScaleFadeAction(), getSetCounterTextAction("2"),
				getCounterScaleFadeAction(), getSetCounterTextAction("1"),
				getCounterScaleFadeAction(),
				changeGameStateAction(GameState.RUNNING));
	}

	private Action changeGameStateAction(final GameState newGameState) {
		return new Action() {

			@Override
			public boolean act(float delta) {
				Gdx.app.log(SavingPlanetEarth.LOG,
						"inside changeGameStateAction(), setting gameState to : "
								+ newGameState);

				gameState = newGameState;

				if (newGameState == GameState.RUNNING) {
					if (stage.getActors().contains(gameOverBGImage, true))
						gameOverBGImage.setVisible(false);
				}

				return true;
			}
		};
	}

	private Action getSetCounterTextAction(final String counterText) {
		return new Action() {

			@Override
			public boolean act(float delta) {
				// set the counter text to "counterText"
				if (!counterLabel.isVisible())
					counterLabel.setVisible(true);

				counterLabel.setText(counterText);
				counterLabel.setWrap(false);

				counterLabel
						.setPosition((stage.getWidth() - counterLabel
								.getTextBounds().width) / 2,
								(stage.getHeight() + counterLabel
										.getTextBounds().height / 2) / 2);
				Gdx.app.log(
						SavingPlanetEarth.LOG,
						counterText + ", pos:" + counterLabel.getX() + ","
								+ counterLabel.getY()
								+ ",  dimensions: textBounds.width = "
								+ counterLabel.getTextBounds().width
								+ " textBounds.height = "
								+ counterLabel.getTextBounds().height);
				counterLabel.getColor().a = 1f;
				return true;
			}
		};
	}

	private Action getCounterScaleFadeAction() {
		// scale (1 to 6)
		// fade (1 to 0)
		// time (counterTime/3)
		return getLabelFadeAction(COUNTER_TIME / 6, COUNTER_TIME / 6);
	}

	/**
	 * returns an action with the following sequence : delay(onTime) >>
	 * alpha(0f, fadeOutTime)
	 * 
	 * @param onTime
	 * @param fadeOutTime
	 * @return
	 */
	private Action getLabelFadeAction(float onTime, float fadeOutTime) {
		return sequence(delay(onTime), alpha(0.2f, fadeOutTime));
	}

	@Override
	public void pause() {
		super.pause();

		// change the gamestate to paused
		gameState = GameState.PAUSED;

		// TODO : add the overlay to dim the screen
		if (gameOverBGImage == null)
			gameOverBGImage = new Image(new TextureRegionDrawable(getAtlas()
					.findRegion("game-screen/gameover-bg-overlay")),
					Scaling.fill);
		gameOverBGImage.getColor().a = 0.3f;
		if (stage.getActors().contains(gameOverBGImage, true)) {
			gameOverBGImage.setVisible(true);
		} else {
			stage.addActor(gameOverBGImage);
			gameOverBGImage.setFillParent(true);
		}

	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		if (gameState == GameState.RESUMING)
			return;

		if (gameState == GameState.PAUSED || gameState == GameState.RUNNING) {
			gameResumeCourse();
		} else {
			gameState = GameState.RUNNING;

			// create the planet and add it to the stage at center
			createAndInitializePlanet(width, height);

			// initialize the fonts
			initFonts();

			// initialize the labels
			initLabels(width, height);

			// cornerImages
			initCornerImages(width, height);

			// ParticleEffectsFactory
			initPEFactory(width, height);

			// METEORS
			initMeteors(width, height);

			// BONUSPACKS
			initBonusPacks(width, height);

			// initialize the finalX and finalY for bonus packs
			initFinalXY(width, height);

			// tmp
			initGuides(width, height);

		}

	}

	private void initPEFactory(int width, int height) {
		ParticleEffectFactory.initialize(width, height);
	}

	private void initFinalXY(int width, int height) {
		starActorFinalX = SCORE_LABEL_CENTER_X;
		starActorFinalY = height - SCORE_LABEL_CENTER_Y;
		healthActorFinalX = width - HEALTH_LABEL_CENTER_X;
		healthActorFinalY = height - SCORE_LABEL_CENTER_Y;
	}

	private void initGuides(int width, int height) {
		scoreLabelCenterHorizontalLine = new Image(new TextureRegionDrawable(
				getAtlas().findRegion("game-screen/horizontal_line")));
		stage.addActor(scoreLabelCenterHorizontalLine);
		scoreLabelCenterHorizontalLine.setWidth(width);
		scoreLabelCenterHorizontalLine.setPosition(0, starActorFinalY);

		scoreLabelCenterVerticalLine = new Image(new TextureRegionDrawable(
				getAtlas().findRegion("game-screen/vertical_line")));
		stage.addActor(scoreLabelCenterVerticalLine);
		scoreLabelCenterVerticalLine.setHeight(height);
		scoreLabelCenterVerticalLine.setPosition(starActorFinalX, 0);

		healthLabelCenterVerticalLine = new Image(new TextureRegionDrawable(
				getAtlas().findRegion("game-screen/vertical_line")));
		stage.addActor(healthLabelCenterVerticalLine);
		healthLabelCenterVerticalLine.setHeight(height);
		healthLabelCenterVerticalLine.setPosition(healthActorFinalX, 0);
	}

	private void initBonusPacks(int width, int height) {

		BonusPackFactory.initialize(width, height, game, stage, getAtlas(),
				profile);
		BonusPackFactory.enqueBonusPacks();

	}

	private void initMeteors(int width, int height) {
		MeteorFactory.initialize(width, height, stage, game);
		if (meteors2d.size == 0) {
			for (int i = 0; i < 4; i++) {
				Gdx.app.log(SavingPlanetEarth.LOG,
						"enqueing meteor for creation");

				// create and add meteor
				MeteorFactory.enqueMeteorCreation(METEOR_CREATION_TIME);
			}

		}

	}

	private void initCornerImages(int width, int height) {
		for (int i = 0; i < 4; i++) {
			Image cornerImage = new Image(new TextureRegionDrawable(getAtlas()
					.findRegion("game-screen/corner")), Scaling.fill);
			stage.addActor(cornerImage);
			cornerImages.add(cornerImage);
			cornerImage.getColor().a = 0f;

			// set location (corner)
			if (i == 0)
				cornerImage.setPosition(-cornerImage.getWidth() / 2,
						-cornerImage.getHeight() / 2);
			else if (i == 1)
				cornerImage.setPosition(width - cornerImage.getWidth() / 2,
						-cornerImage.getHeight() / 2);
			else if (i == 2)
				cornerImage.setPosition(width - cornerImage.getWidth() / 2,
						height - cornerImage.getHeight() / 2);
			else
				cornerImage.setPosition(-cornerImage.getWidth() / 2, height
						- cornerImage.getHeight() / 2);

		}

	}

	private void initLabels(int width, int height) {
		initializeCounterLabel(width, height);
		initializeHealthLabel(width, height);
		initializeScoreLabel(width, height);
		initializePauseLabel(width, height);

		// tmp
		initializeFPSLabel(width, height);
	}

	private void initializeFPSLabel(int width, int height) {
		// fps label
		fpsLabel = new Label("FPS: ", getSkin());
		stage.addActor(fpsLabel);
		fpsLabel.setPosition(5, 0);

	}

	private void initializePauseLabel(int width, int height) {
		// pauseLabel
		// pauseLabel = new DistanceFontLabel(" l l ", labelStyle);
		pauseLabel = new Label(" II ", getSkin());
		pauseLabel.setTouchable(Touchable.enabled);
		stage.addActor(pauseLabel);
		pauseLabel.setPosition(width - 40, 0);

	}

	private void initializeScoreLabel(int width, int height) {
		// scoreLabel
		// scoreLabel = new DistanceFontLabel("", labelStyle);
		scoreLabel = new Label("", getSkin());
		stage.addActor(scoreLabel);
		scoreLabel.setPosition(5, height - 15);

	}

	private void initializeHealthLabel(int width, int height) {
		// healthLabel
		// healthLabel = new DistanceFontLabel("", labelStyle);
		// stage.addActor(healthLabel);
		healthLabel = new Label("", getSkin());
		stage.addActor(healthLabel);
		healthLabel.setPosition(width - 165, height - 15);
		healthLabel.setWidth(160f);
		healthLabel.setAlignment(Align.right);

	}

	private void initFonts() {
		initializeCounterFont();
		initializeDefaultFont();
	}

	private void initializeDefaultFont() {
		labelStyle = new LabelStyle(FontFactory.getDefaultFont(), Color.WHITE);
	}

	private void initializeCounterFont() {
		counterStyle = new LabelStyle(FontFactory.getCounterFont(), Color.RED);
	}

	private void createAndInitializePlanet(float width, float height) {
		planet2d = Planet2D.create(getAtlas());

		planet2d.setPosition(width / 2 - planet2d.getWidth() / 2, height / 2
				- planet2d.getHeight() / 2);

		// add the planet actor to the stage
		stage.addActor(planet2d);

	}

	private void initializeCounterLabel(float width, float height) {
		// counterLabel
		counterLabel = new Label("", counterStyle);
		counterLabel.setOrigin(counterLabel.getWidth() / 2,
				counterLabel.getHeight() / 2);
		stage.addActor(counterLabel);
		counterLabel.setPosition(width / 2 - counterLabel.getWidth() / 2,
				height / 2 - counterLabel.getHeight() / 2);
	}

	private boolean updateHighscores() {
		Array<Integer> highScores = profile.getHighScores();
		// check if the current score is higher than any of the 5 highscores
		boolean shouldEnterScore = false;
		for (Integer highScore : highScores) {
			if (highScore < game.gameStats.score)
				shouldEnterScore = true;
		}

		if (shouldEnterScore) {
			// update the highscores accordingly
			// sort the highscores array
			highScores.sort();
			highScores.reverse();
			// remove the smallest element
			highScores.removeIndex(highScores.size - 1);
			// insert this score
			highScores.add((int) game.gameStats.score);
			// sort the array
			highScores.sort();
			highScores.reverse();

			// initialize the newHighScoreLabel for GameOver overlay
			newHighScoreLabel = new Label("New HighScore !!", labelStyle);
		}

		return shouldEnterScore;

	}

	@Override
	protected String getName() {
		return "GameScreen";
	}

	/**
	 * This method is called from the Planet2D class when the health of the
	 * planet becomes 0
	 */
	public static void setGameOver() {
		gameState = GameState.GAMEOVER;

	}

	private void initializeGameOverActors(boolean isNewHighScore) {
		// initialize the actors

		if (gameOverBGImage == null)
			gameOverBGImage = new Image(new TextureRegionDrawable(getAtlas()
					.findRegion("game-screen/gameover-bg-overlay")),
					Scaling.fill);

		Label seperatorLabel1 = new Label("----------------------------------",
				labelStyle);
		Label seperatorLabel2 = new Label("----------------------------------",
				labelStyle);

		if (table == null)
			table = new Table(getSkin());

		if (gameOverLabel == null)
			gameOverLabel = new Label("GAME OVER", labelStyle);

		if (finalScoreLabel == null)
			finalScoreLabel = new Label("Final Score : ", labelStyle);
		finalScoreLabel.setText(finalScoreLabel.getText()
				+ Integer.toString((int) game.gameStats.score));

		if (isNewHighScore)
			if (newHighScoreLabel == null)
				newHighScoreLabel = new Label("New High Score !!", labelStyle);

		if (creditsEarnedLabel == null)
			creditsEarnedLabel = new Label("Credits Earned : ", labelStyle);
		creditsEarnedLabel.setText(creditsEarnedLabel.getText()
				+ Integer.toString(calculateCreditsEarned()));

		if (goToMainMenuButton == null)
			goToMainMenuButton = new TextButton("Go to Main Menu", getSkin());
		goToMainMenuButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);
				game.setScreen(new MenuScreen(game));
			}
		});

		// add and position the actors
		gameOverBGImage.getColor().a = 0.5f;
		stage.addActor(gameOverBGImage);

		table.setFillParent(true);
		stage.addActor(table);

		table.add(gameOverLabel);
		table.row();
		table.add(seperatorLabel1).spaceBottom(0);
		table.row();
		table.add(finalScoreLabel).spaceBottom(0);
		table.row();
		if (isNewHighScore) {
			table.add(newHighScoreLabel).spaceBottom(0);
			table.row();
		}
		table.add(creditsEarnedLabel).spaceBottom(15);
		table.row();
		table.add(seperatorLabel2).spaceBottom(25);
		table.row();
		table.add(goToMainMenuButton).size(350, 60);
	}

	private int calculateCreditsEarned() {
		// credits earned = 1% of the score
		return (int) (game.gameStats.score * 0.01);
	}

	/**
	 * creates and adds a collision particle effect at the required location
	 * 
	 * @param x
	 * @param y
	 */
	private void createAndAddCollisionActor(float x, float y) {
		// create the particle effect
		Gdx.app.log(SavingPlanetEarth.LOG, "creating collision effect");

		// create the collision effect actor
		MeteorCollision2D meteorCollision2D = MeteorCollision2D.create(x, y);

		// add this actor to the stage
		stage.addActor(meteorCollision2D);
	}
}
