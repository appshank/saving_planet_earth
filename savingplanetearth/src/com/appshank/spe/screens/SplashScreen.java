package com.appshank.spe.screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.factories.FontFactory;
import com.appshank.spe.services.MusicManager.SPEMusic;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Shows a splash image and moves on to the next screen.
 */
public class SplashScreen extends AbstractScreen {

	private Texture splashImageTexture;
	private TextureRegion splashImageTextureRegion;
	private Image splashImage;
	private Label loadingLabel;
	private Slider loadingSlider;

	private boolean firstLoad = true;

	public SplashScreen(SavingPlanetEarth game) {
		super(game);
	}

	@Override
	public void show() {
		super.show();

		// start playing the menu music
		game.getMusicManager().play(SPEMusic.MENU);

		// retrieve the splash image's region from the data folder
		splashImageTexture = new Texture(
				Gdx.files.internal("data/splash-image.png"), true);
		splashImageTextureRegion = new TextureRegion(splashImageTexture, 0, 0,
				512, 300);

		// create the splash image actor and set its size
		splashImage = new Image(splashImageTextureRegion);
		splashImage.setFillParent(true);

		// make the image completely transparent so that the fade-in effect can
		// work properly
		splashImage.getColor().a = 0;

		// configure the fade-in and out effects on the image actor
		splashImage.addAction(fadeIn(0.75f));

		// add the actor to the stage
		stage.addActor(splashImage);

		loadingLabel = new Label("loading : ", getSkin());
		stage.addActor(loadingLabel);
		loadingLabel.setPosition(300, 240);

		loadingSlider = new Slider(0, 100f, 1f, false, getSkin());
		stage.addActor(loadingSlider);
		loadingSlider.setPosition(400, 240);
		loadingSlider.setValue(0f);
	}

	@Override
	public void render(float delta) {
		// update the loading label
		if (game.getAssetsManager().update() && firstLoad) {
			// we are done loading, move to another screen
			Gdx.app.log(SavingPlanetEarth.LOG,
					"loading done in "
							+ (TimeUtils.nanoTime() - game.getAssetsManager()
									.getStartTime()) / 1000000000f + " seconds");

			// set the atlas to be used by the screens
			AbstractScreen.atlas = game.getAssetsManager().get(
					"image-atlases/pages.atlas", TextureAtlas.class);

			// initialize the font factory
			FontFactory.initialize(game);

			splashImage.addAction(sequence(delay(0.2f), fadeOut(0.75f),
					new Action() {
						@Override
						public boolean act(float delta) {
							game.setScreen(new MenuScreen(game));
							return true;
						}
					}));

			firstLoad = false; // so that we dont enter this code block again
		}

		float progress = game.getAssetsManager().getProgress();
		loadingLabel.setText("loading : " + progress * 100f + " %");
		loadingSlider.setValue(progress * 100f);

		// render
		super.render(delta);
	}

	@Override
	protected String getName() {
		return "SplashScreen";
	}

	@Override
	public void dispose() {
		splashImageTexture.dispose();
		super.dispose();
	}

}
