package com.appshank.spe.screens;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.factories.FontFactory;
import com.appshank.spe.services.MusicManager.SPEMusic;
import com.appshank.spe.services.SoundManager.SPESound;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * A simple options screen.
 */
public class OptionsScreen extends AbstractScreen {

	private Table table;
	private Label volumeValue;

	public OptionsScreen(SavingPlanetEarth game) {
		super(game);
	}

	@Override
	public void show() {
		super.show();

		// retrieve the skin created on the AbstractScreen class
		Skin skin = super.getSkin();

		// create the table actor
		table = addTableActor(skin);

		LabelStyle screenTitleStyle = new LabelStyle(
				FontFactory.getScreenTitleFont(), Color.WHITE);
		Label screenTitleLabel = new Label("OPTIONS", screenTitleStyle);
		table.add(screenTitleLabel).colspan(3);

		// add the sound effects checkbox
		table.row();
		final CheckBox soundEffectsCheckbox = new CheckBox("", skin);
		soundEffectsCheckbox.setChecked(game.getPreferencesManager()
				.isSoundEnabled());
		soundEffectsCheckbox.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				boolean enabled = soundEffectsCheckbox.isChecked();
				game.getPreferencesManager().setSoundEnabled(enabled);
				game.getSoundManager().setEnabled(enabled);
				game.getSoundManager().play(SPESound.CLICK);
			}
		});
		table.add("Sound Effects");
		table.add(soundEffectsCheckbox).colspan(2).left();

		// music checkbox
		table.row();
		final CheckBox musicCheckbox = new CheckBox("", skin);
		musicCheckbox.setChecked(game.getPreferencesManager().isMusicEnabled());
		musicCheckbox.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				boolean enabled = musicCheckbox.isChecked();
				game.getPreferencesManager().setMusicEnabled(enabled);
				game.getMusicManager().setEnabled(enabled);
				game.getSoundManager().play(SPESound.CLICK);

				// if the music is now enabled, start playing the menu music
				if (enabled)
					game.getMusicManager().play(SPEMusic.MENU);

			}
		});
		table.add("Music");
		table.add(musicCheckbox).colspan(2).left();

		// volume slider
		// range is [0.0,1.0]; step is 0.1f
		table.row();
		Slider volumeSlider = new Slider(0f, 1f, 0.1f, false, skin);
		volumeSlider.setValue(game.getPreferencesManager().getVolume());
		volumeSlider.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				float value = ((Slider) actor).getValue();
				game.getPreferencesManager().setVolume(value);
				game.getMusicManager().setVolume(value);
				game.getSoundManager().setVolume(value);
				updateVolumeLabel();
			}
		});
		// create the volume label
		volumeValue = new Label("", skin);
		updateVolumeLabel();
		table.add("Volume");
		table.add(volumeSlider);
		table.add(volumeValue).width(40);

		// vibration
		// add the sound effects checkbox
		table.row();
		final CheckBox vibrationCheckbox = new CheckBox("", skin);
		vibrationCheckbox.setChecked(game.getPreferencesManager()
				.isVibrationEnabled());
		vibrationCheckbox.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				boolean enabled = vibrationCheckbox.isChecked();
				game.getPreferencesManager().setVibrationEnabled(enabled);
			}
		});
		table.add("Vibration").spaceBottom(SPACE_ABOVE_BUTTONS);
		table.add(vibrationCheckbox).colspan(2).left();

		// add the start-game button sized 300x60 with a margin-bottom of 10
		// units
		table.row();
		// button "Back to Menu"
		TextButton mainMenuButton = new TextButton("Back to Main Menu", skin);
		mainMenuButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log(SavingPlanetEarth.LOG, "mainMenuButton clicked");
				super.clicked(event, x, y);
				game.getSoundManager().play(SPESound.CLICK);
				game.setScreen(new MenuScreen(game));
			}
		});
		table.add(mainMenuButton).size(350, 60).colspan(3);

	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		// add the credits label
		addCreditsLabel(width, height);
	}

	/**
	 * Updates the volume label next to the slider.
	 */
	private void updateVolumeLabel() {
		float volume = game.getPreferencesManager().getVolume() * 100;
		volumeValue.setText(Integer.toString((int) volume));
	}

	@Override
	protected String getName() {
		return "OptionsScreen";
	}

}
