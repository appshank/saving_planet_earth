package com.appshank.spe.domain;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.factories.BonusPackFactory;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.OrderedMap;

/**
 * The profile of the player
 * <p>
 * 
 * @author AppShank
 * @param <T>
 * 
 */
public class Profile {

	private int credits;

	private ProfileStats profileStats;

	/**
	 * contains the types (int) of the purchased bonus packs
	 */
	private Array<Integer> purchasedBonusPackTypes;
	/**
	 * contains the levels (int) of the purchased bonus packs
	 */
	private Array<Integer> purchasedBonusPackLevels;

	public Profile() {
		credits = 1000;

		purchasedBonusPackTypes = new Array<Integer>();
		purchasedBonusPackLevels = new Array<Integer>();

		buy(BonusPackFactory.getHealthPack());
		buy(BonusPackFactory.getStarPack());
		buy(BonusPackFactory.getShieldPack());

		profileStats = new ProfileStats();
	}

	/**
	 * Buys the given item.
	 */
	public boolean buy(BonusPack bonusPack) {
		if (canBuy(bonusPack)) {
			Gdx.app.log(SavingPlanetEarth.LOG, "Buying BonusPack: " + bonusPack);

			// add the bonus pack to the purchased bonus packs list
			getPurchasedBonusPacks().add(bonusPack.getType());
			credits -= BonusPackFactory.getBonusPack(bonusPack.getType())
					.getPrice();

			// initialize the level of this bonus pack to 1
			purchasedBonusPackLevels.add(1);

			Gdx.app.log(SavingPlanetEarth.LOG, "Credits available: " + credits);

			return true;
		} else {
			Gdx.app.log(SavingPlanetEarth.LOG, "No credits to buy bonus pack: "
					+ bonusPack);
			return false;
		}
	}

	/**
	 * Checks whether the given item can be bought.
	 */
	public boolean canBuy(BonusPack bonusPack) {
		if (purchasedBonusPackTypes.contains(bonusPack.getType(), false)) {
			return false;
		}
		if (BonusPackFactory.getBonusPack(bonusPack.getType()).getPrice() > credits) {
			return false;
		}
		return true;
	}

	/**
	 * Retrieves the amount of credits the player has.
	 */
	public int getCredits() {
		return credits;
	}

	/**
	 * 
	 * @return
	 */
	public void setCredits(int credits) {
		this.credits = credits;
	}

	public Array<Integer> getHighScores() {
		return profileStats.highScores;
	}

	public Array<Integer> getPurchasedBonusPacks() {
		return purchasedBonusPackTypes;
	}

	public Array<Integer> getPurchasedBonusPackLevels() {
		return purchasedBonusPackLevels;
	}

	private void setPurchasedBonusPackTypes(
			Array<Integer> purchasedBonusPackTypes) {
		this.purchasedBonusPackTypes = purchasedBonusPackTypes;
	}

	/**
	 * Notifies the score. <code>true</code> if its a high score.
	 */
	public boolean notifyScore(int score) {
		boolean isHighScore = true;

		for (Integer highScore : profileStats.highScores) {
			if (score < highScore) {
				isHighScore = false;
				return isHighScore;
			}
		}
		return isHighScore;
	}

	/**
	 * Increases the credits by the amount passed as argument
	 */
	public void addCredits(int credits) {
		this.credits += credits;
	}

	/**
	 * Updates the profileStats as per the gameStats passed as argument
	 */
	public void updateProfileStats(GameStats gameStats) {
		profileStats.updateStats(gameStats);
	}
	
}
