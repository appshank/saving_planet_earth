package com.appshank.spe.domain;

/**
 * A bonus pack that gives some bonus (health, shield etc.)
 * <p>
 * 
 * @author AppShank
 * 
 */
public interface BonusPack {

	public static final int HEALTH = 1;
	public static final int SHIELD = 2;
	public static final int STAR = 3;

	/**
	 * return an int to indicate the type of bonus pack
	 * <p>
	 * 
	 * @return {@link BonusPack#HEALTH} or {@link BonusPack#SHIELD}
	 */
	public int getType();

	/**
	 * return the number of credits required to enable this bonus pack
	 * 
	 * @return
	 */
	public int getPrice();

	/**
	 * return the number of credits required to upgrade this bonus pack so as to
	 * decrease the time between consecutive appreances
	 * 
	 * @return
	 */
	public int getUpgradeCost();

}
