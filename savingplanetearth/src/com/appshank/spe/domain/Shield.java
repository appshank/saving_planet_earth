package com.appshank.spe.domain;

public enum Shield implements Item {
	MS("Magnetic Shield", 50);

	private final String name;
	private final int damageAbsorptionCapability;

	private Shield(String name, int damageAbsorptionCapability) {
		this.name = name;
		this.damageAbsorptionCapability = damageAbsorptionCapability;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getSimpleName() {
		return "shield-" + name().replaceAll("_", "-").toLowerCase();
	}

	@Override
	public String toString() {
		return name + "(" + damageAbsorptionCapability + ")";
	}

	public int getDamageAbsorptionCapability() {
		return damageAbsorptionCapability;
	}

}
