package com.appshank.spe.domain;

import com.appshank.spe.SavingPlanetEarth;
import com.badlogic.gdx.Gdx;

public class Planet {

	private PlanetModel planetModel;
	private Shield shield;
	private Gun gun;

	public PlanetModel getPlanetModel() {
		return planetModel;
	}

	public Shield getShield() {
		return shield;
	}

	/**
	 * Checks whether the planet contains the given item.
	 */
	public boolean contains(Item item) {
		if (item == null)
			return false;
		return (item.equals(planetModel) || item.equals(shield));
	}

	/**
	 * Installs the given item on the planet.
	 * <p>
	 * No credit verification is done here.
	 */
	public void install(Item item) {
		Gdx.app.log(SavingPlanetEarth.LOG, "Installing item: " + item);
		if (item instanceof PlanetModel) {
			planetModel = (PlanetModel) item;
		} else if (item instanceof Shield) {
			shield = (Shield) item;
		} else if (item instanceof Gun) {
			gun = (Gun) item;
		} else {
			throw new IllegalArgumentException("Unknown item: " + item);
		}
	}

	public int getDia() {
		return (int) planetModel.getDia();
	}

	public void decreaseHealth(int decrementValue) {
		planetModel.decreaseHealth(decrementValue);
	}

	public int getHealth() {
		return planetModel.getHealth();
	}

	public void removeShield() {
		shield = null;
	}
}
