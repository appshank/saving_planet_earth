package com.appshank.spe.domain;

/**
 * an item that can be installed on the planet
 * <p>
 * 
 * @author AppShank
 * 
 */
public interface Item {
	/**
	 * Retrieves the name of this item.
	 */
	String getName();

	/**
	 * Retrieves a simple representation of the item's name, useful for creating
	 * a convention for file names.
	 */
	String getSimpleName();
}
