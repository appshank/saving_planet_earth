package com.appshank.spe.domain;

import com.badlogic.gdx.utils.Array;

/**
 * Class to hold the stats related to profile and game
 * 
 * @author AppShank
 * 
 */
public class ProfileStats {
	

	private static final int HIGHSCORES_COUNT = 5;

	// highscores
	Array<Integer> highScores;

	// total number of games played
	int nGamesPlayed;

	// total duration played
	float totalGameDuration;

	// total BonusPacks appeared
	int nBonusPacksAppeared;

	// total BonusPacks absorbed
	int nBonusPacksAbsorbed;

	// total HealthPacks appeared
	int nHealthPacksAppeared;

	// total HealthPacks absorbed
	int nHealthPacksAbsorbed;

	// total StarPacks appeared
	int nStarPacksAppeared;

	// total StarPacks absorbed
	int nStarPacksAbsorbed;

	// total ShieldPacks appeared
	int nShieldPacksAppeared;

	// total ShieldPacks absorbed
	int nShieldPacksAbsorbed;

	public ProfileStats() {
		highScores = new Array<Integer>(HIGHSCORES_COUNT);
		for (int i = 0; i < HIGHSCORES_COUNT; i++) {
			highScores.add(0);
		}

	}

	// update ProfileStats as per current GameStats
	// to be called once the game is over
	public void updateStats(GameStats gameStats) {
		nGamesPlayed += 1;
		totalGameDuration += gameStats.gameDuration;
		nBonusPacksAppeared += gameStats.nBonusPacksAppeared;
		nBonusPacksAbsorbed += gameStats.nBonusPacksAbsorbed;
		nHealthPacksAppeared += gameStats.nHealthPacksAppeared;
		nStarPacksAppeared += gameStats.nStarPacksAppeared;
		nShieldPacksAppeared += gameStats.nShieldPacksAppeared;
		nHealthPacksAbsorbed += gameStats.nHealthPacksAbsorbed;
		nStarPacksAbsorbed += gameStats.nStarPacksAbsorbed;
		nShieldPacksAbsorbed += gameStats.nShieldPacksAbsorbed;
	}
}
