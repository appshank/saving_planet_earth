package com.appshank.spe.domain;

public enum Gun implements Item {
	DG("Default Gun", 4);

	private final String name;
	private final int rounds;

	private Gun(String name, int rounds) {
		this.name = name;
		this.rounds = rounds;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getSimpleName() {
		return "gun-" + name().replaceAll("_", "-").toLowerCase();
	}

}
