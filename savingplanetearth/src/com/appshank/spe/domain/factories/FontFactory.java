package com.appshank.spe.domain.factories;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.services.MyAssetsManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * helper class to retrieve fonts. Note that the fonts are loaded via
 * "MyAssetsManager" and not this factory. This factory just provides a layer of
 * abstraction between the game screens and the font source
 * 
 * @author AppShank
 * 
 */
public class FontFactory {
	private enum Font_Category {
		GAME_TITLE, SCREEN_TITLE, GAMEOVER_TITLE, DEFAULT, COUNTER;
	}

	public static final String FONT_FOLDER = MyAssetsManager.FONT_FOLDER;
	public static final String COUNTER_FONT = MyAssetsManager.COUNTER_FONT;
	public static final String DEFAULT_FONT = MyAssetsManager.DEFAULT_FONT;
	public static final String GAME_OVER_FONT = MyAssetsManager.GAME_OVER_FONT;
	public static final String SCREEN_TITLE_FONT = MyAssetsManager.SCREEN_TITLE_FONT;
	public static final String GAME_TITLE_FONT = MyAssetsManager.GAME_TITLE_FONT;

	private static SavingPlanetEarth game;

	// GAME TITLE

	// SCREEN TITLES

	// GAMEOVER TITLE

	// DEFAULT

	// COUNTER

	public static void initialize(SavingPlanetEarth spe) {
		// store the game instance
		game = spe;
	}

	private static BitmapFont getFont(Font_Category type) {
		BitmapFont font = null;

		if (game == null)
			throw new IllegalStateException("FontFactory not initialized !");

		switch (type) {
		case GAME_TITLE:
			font = game.getAssetsManager().getFont(GAME_TITLE_FONT);
			font.setColor(1, 0, 0, 1);
			break;
		case SCREEN_TITLE:
			font = game.getAssetsManager().getFont(SCREEN_TITLE_FONT);
			Gdx.app.log(SavingPlanetEarth.LOG, "screen title font : scaleX : "
					+ font.getScaleX() + " scaleY : " + font.getScaleY());
			break;
		case GAMEOVER_TITLE:
			font = game.getAssetsManager().getFont(GAME_OVER_FONT);
			break;
		case COUNTER:
			font = game.getAssetsManager().getFont(COUNTER_FONT);
			break;
		case DEFAULT:
		default:
			font = game.getAssetsManager().getFont(DEFAULT_FONT);
			break;
		}

		return font;
	}

	public static BitmapFont getGameTitleFont() {
		return getFont(Font_Category.GAME_TITLE);
	}

	public static BitmapFont getScreenTitleFont() {
		return getFont(Font_Category.SCREEN_TITLE);
	}

	public static BitmapFont getGameOverTitleFont() {
		return getFont(Font_Category.GAMEOVER_TITLE);
	}

	public static BitmapFont getDefaultFont() {
		return getFont(Font_Category.DEFAULT);
	}

	public static BitmapFont getCounterFont() {
		return getFont(Font_Category.COUNTER);
	}
}
