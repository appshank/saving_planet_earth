package com.appshank.spe.domain.factories;

import java.util.Random;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.Meteor;
import com.appshank.spe.screens.GameScreen;
import com.appshank.spe.screens.scene2d.Meteor2D;
import com.appshank.spe.utils.Corner;
import com.appshank.spe.utils.MeteorSpeedCalculator;
import com.appshank.spe.utils.ResolutionIndependenceHelper;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;

/**
 * A factory for creating Meteors
 * <p>
 * 
 * @author AppShank
 * 
 */
public class MeteorFactory {

	private static Random random;

	// stage
	private static Stage stage;

	// game
	private static SavingPlanetEarth game;

	// the deimensions of the stage
	private static float stageWidth, stageHeight;

	// the creation queue for the meteors
	private static Array<Float> creationTimers;
	private static Array<Corner> creationCorners;

	// initialize the factory variables
	public static void initialize(float sw, float sh, Stage gameScreenStage,
			SavingPlanetEarth game) {
		stageWidth = sw;
		stageHeight = sh;
		random = new Random();
		creationTimers = new Array<Float>();
		creationCorners = new Array<Corner>();

		stage = gameScreenStage;
		MeteorFactory.game = game;

		// initialize speeds
		initSpeeds(stageWidth, stageHeight);

		// initialize collision radius of meteors
		initMeteorCollsionRadius();
	}

	private static void initMeteorCollsionRadius() {
		Meteor.initDia(ResolutionIndependenceHelper.getScalingFactor());
	}

	private static void initSpeeds(float stageWidth2, float stageHeight2) {
		MeteorSpeedCalculator speedCalculator = new MeteorSpeedCalculator(
				stageWidth, stageHeight);

		Meteor2D.initSpeeds(speedCalculator);
	}

	// enque a meteor for creation after x seconds
	public static void enqueMeteorCreation(float waitTime) {
		creationTimers.add(waitTime);

		float randomizer = random.nextFloat();
		Corner corner;
		if (randomizer < 0.25)
			corner = Corner.BOTTOM_LEFT;
		else if (randomizer < 0.5)
			corner = Corner.BOTTOM_RIGHT;
		else if (randomizer < 0.75)
			corner = Corner.TOP_RIGHT;
		else
			corner = Corner.TOP_LEFT;

		creationCorners.add(corner);

	}

	public static void update(float delta) {

		// update the timers
		for (int i = 0; i < creationTimers.size; i++) {
			creationTimers.set(i, creationTimers.get(i) - delta);
		}

		// create meteors for which timer is up
		for (Float timer : creationTimers) {
			if (timer <= 0f) {
				Corner corner = creationCorners.removeIndex(creationTimers
						.indexOf(timer, false));
				creationTimers.removeValue(timer, false);
				createAndAddMeteor(corner);
			}
		}

	}

	private static void createAndAddMeteor(Corner corner) {

		Gdx.app.log(SavingPlanetEarth.LOG, "creating meteor");

		Meteor2D meteor2d = Meteor2D.create(corner, stageWidth, stageHeight);
		GameScreen.meteors2d.add(meteor2d);
		stage.addActor(meteor2d);
	}

	public static Array<Corner> getCorners() {
		return creationCorners;
	}

}
