package com.appshank.spe.domain.factories;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.repeat;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import java.util.Random;

import com.appshank.spe.SavingPlanetEarth;
import com.appshank.spe.domain.BonusPack;
import com.appshank.spe.domain.HealthPack;
import com.appshank.spe.domain.Profile;
import com.appshank.spe.domain.ShieldPack;
import com.appshank.spe.domain.StarPack;
import com.appshank.spe.screens.GameScreen;
import com.appshank.spe.screens.scene2d.BonusPack2D;
import com.appshank.spe.screens.scene2d.HealthPack2D;
import com.appshank.spe.screens.scene2d.ShieldPack2D;
import com.appshank.spe.screens.scene2d.StarPack2D;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

/**
 * A factory for bonus packs
 * <p>
 * 
 * @author AppShank
 * 
 */
public class BonusPackFactory {
	private static HealthPack healthPack;
	private static ShieldPack shieldPack;
	private static StarPack starPack;

	public static final float FINAL_BONUS_PACK_ALPHA = 0.9f;
	private static final float HEALTH_PACK_LIFE = 2f;
	private static final float SHIELD_PACK_LIFE = 2f;
	private static final float STAR_PACK_LIFE = 2.5f;

	private static Random random;

	// the timer for bonuspack creation
	private static final float MIN_TIMER_HEALTH_PACK = 25f;
	private static final float MAX_TIMER_HEALTH_PACK = 35f;
	private static final float MIN_TIMER_STAR_PACK = 15f;
	private static final float MAX_TIMER_STAR_PACK = 20f;
	private static final float MIN_TIMER_SHIELD_PACK = 50f;
	private static final float MAX_TIMER_SHIELD_PACK = 55f;

	// the multipliers for the bonuspack creation timers as per the updates
	// level
	private static float MULTIPLIER_TIMER_HEALTH_PACK = 1f;
	private static float MULTIPLIER_TIMER_STAR_PACK = 1f;
	private static float MULTIPLIER_TIMER_SHIELD_PACK = 1f;

	// the creation queue for the bonuspacks
	private static Array<Float> creationTimers;
	private static Array<Integer> purchasedBonusPackTypes;

	// stage
	private static Stage stage;

	// game
	private static SavingPlanetEarth game;

	// the dimensions of the stage
	private static float stageWidth, stageHeight;

	private static TextureAtlas atlas;

	// initialize the factory variables
	public static void initialize(float sw, float sh, SavingPlanetEarth game,
			Stage gameScreenStage, TextureAtlas textureAtlas, Profile profile) {
		stageWidth = sw;
		stageHeight = sh;
		random = new Random();

		// get the number and type of bonus packs that are available to this
		// profile
		purchasedBonusPackTypes = profile.getPurchasedBonusPacks();
		creationTimers = new Array<Float>(purchasedBonusPackTypes.size);
		for (int i = 0; i < purchasedBonusPackTypes.size; i++)
			creationTimers.add(-100f);

		// set the values of the multipliers as per the current updates levels
		setMultipliersAsPerProfile(profile);

		Gdx.app.log(SavingPlanetEarth.LOG, "MULTIPLIER_TIMER_HEALTH_PACK : "
				+ MULTIPLIER_TIMER_HEALTH_PACK);
		Gdx.app.log(SavingPlanetEarth.LOG, "MULTIPLIER_TIMER_STAR_PACK : "
				+ MULTIPLIER_TIMER_STAR_PACK);
		Gdx.app.log(SavingPlanetEarth.LOG, "MULTIPLIER_TIMER_SHIELD_PACK : "
				+ MULTIPLIER_TIMER_SHIELD_PACK);

		stage = gameScreenStage;
		BonusPackFactory.game = game;
		atlas = textureAtlas;
	}

	private static void setMultipliersAsPerProfile(Profile profile) {
		// get the current update levels from the profile
		Array<Integer> purchasedBonusPackLevels = profile
				.getPurchasedBonusPackLevels();

		// set the multipliers as per the update levels
		int numberOfPurchasedBonusPackTypes = purchasedBonusPackTypes.size;
		for (int i = 0; i < numberOfPurchasedBonusPackTypes; i++) {
			int bonusPackType = purchasedBonusPackTypes.get(i);
			int bonusPackLevel = purchasedBonusPackLevels.get(i);
			setMultiplier(bonusPackType, bonusPackLevel);
		}

	}

	private static void setMultiplier(int bonusPackType, int bonusPackLevel) {
		// TEMP : levels (1-10) => multiplier (1.0-2.0)
		int MIN_LEVEL = 1;
		int MAX_LEVEL = 10;
		float MIN_MULTIPLIER = 1f;
		float MAX_MULTIPLIER = 2f;

		float multiplierIncrementPerLevel = (MAX_MULTIPLIER - MIN_MULTIPLIER)
				/ MAX_LEVEL;

		float multiplierAsPerLevel = MIN_LEVEL + bonusPackLevel
				* multiplierIncrementPerLevel;

		switch (bonusPackType) {
		case BonusPack.HEALTH:
			MULTIPLIER_TIMER_HEALTH_PACK = multiplierAsPerLevel;
			break;
		case BonusPack.STAR:
			MULTIPLIER_TIMER_STAR_PACK = multiplierAsPerLevel;
			break;
		case BonusPack.SHIELD:
			MULTIPLIER_TIMER_SHIELD_PACK = multiplierAsPerLevel;
			break;
		}
	}

	public static HealthPack getHealthPack() {
		if (healthPack == null)
			healthPack = new HealthPack();
		return healthPack;
	}

	public static ShieldPack getShieldPack() {
		if (shieldPack == null)
			shieldPack = new ShieldPack();
		return shieldPack;
	}

	public static BonusPack getStarPack() {
		if (starPack == null)
			starPack = new StarPack();
		return starPack;
	}

	public static BonusPack getBonusPack(int bonusPackType) {
		if (bonusPackType == BonusPack.HEALTH)
			return getHealthPack();
		else if (bonusPackType == BonusPack.SHIELD)
			return getShieldPack();
		else if (bonusPackType == BonusPack.STAR)
			return getStarPack();
		else
			throw (new IllegalArgumentException(
					"Only implementations of BonusPack can be passed as argument to this function"));
	}

	public static void update(float delta) {

		// update the timers
		for (int i = 0; i < creationTimers.size; i++) {
			creationTimers.set(i, creationTimers.get(i) - delta);
		}

		// create bonusPacks for which timer is up
		for (Float timer : creationTimers) {
			if (timer <= 0f) {
				// random quadrant
				float centerX, centerY;
				float randomizer = random.nextFloat();
				if (randomizer < 0.25) {
					// I
					centerX = stageWidth / 4;
					centerY = stageHeight / 4;
				} else if (randomizer < 0.5) {
					// II
					centerX = stageWidth * 3 / 4;
					centerY = stageHeight / 4;
				} else if (randomizer < 0.75) {
					// III
					centerX = stageWidth * 3 / 4;
					centerY = stageHeight * 3 / 4;
				} else {
					// IV
					centerX = stageWidth / 4;
					centerY = stageHeight * 3 / 4;
				}

				// create the bonus pack
				BonusPack bonusPack = getBonusPack(purchasedBonusPackTypes
						.get(creationTimers.indexOf(timer, false)));
				Gdx.app.log(SavingPlanetEarth.LOG, "creating BonusPack type"
						+ bonusPack.getType() + " at " + centerX + ","
						+ centerY);
				BonusPack2D bonusPack2D = null;
				if (bonusPack.getType() == BonusPack.HEALTH) {
					Drawable drawable = new TextureRegionDrawable(
							atlas.findRegion("game-screen/health-pack"));
					bonusPack2D = new HealthPack2D(centerX, centerY, drawable,
							HEALTH_PACK_LIFE);
				} else if (bonusPack.getType() == BonusPack.STAR) {
					Drawable drawable = new TextureRegionDrawable(
							atlas.findRegion("game-screen/star-pack"));
					bonusPack2D = new StarPack2D(centerX, centerY, drawable,
							STAR_PACK_LIFE);
				} else if (bonusPack.getType() == BonusPack.SHIELD) {
					Drawable drawable = new TextureRegionDrawable(
							atlas.findRegion("game-screen/shield-pack"));
					bonusPack2D = new ShieldPack2D(centerX, centerY, drawable,
							SHIELD_PACK_LIFE);
				}

				// add it to the stage
				stage.addActor(bonusPack2D);
				GameScreen.activeBonusPacks.add(bonusPack2D);
				bonusPack2D.addAction(getBlinkingAction());

				// update the gameStats
				game.gameStats.addBonusPackAppeared(bonusPack.getType());

				// enque another bonus pack
				enqueBonusPack(bonusPack.getType());
			}
		}
	}

	private static Action getBlinkingAction() {
		return sequence(repeat(
				4,
				sequence(alpha(FINAL_BONUS_PACK_ALPHA, HEALTH_PACK_LIFE / 8),
						alpha(0f, HEALTH_PACK_LIFE / 8))));
	}

	public static void enqueBonusPacks() {
		// all types
		for (Integer bonusPackType : purchasedBonusPackTypes) {
			enqueBonusPack(bonusPackType);
		}
	}

	public static void enqueBonusPack(int bonusPackType) {
		// initialize timer
		float minTimer = 0;
		float maxTimer = 0;
		if (bonusPackType == BonusPack.HEALTH) {
			minTimer = MIN_TIMER_HEALTH_PACK;
			maxTimer = MAX_TIMER_HEALTH_PACK;
		} else if (bonusPackType == BonusPack.STAR) {
			minTimer = MIN_TIMER_STAR_PACK;
			maxTimer = MAX_TIMER_STAR_PACK;
		} else if (bonusPackType == BonusPack.SHIELD) {
			minTimer = MIN_TIMER_SHIELD_PACK;
			maxTimer = MAX_TIMER_SHIELD_PACK;
		}
		float creationTimer = MathUtils.random(minTimer, maxTimer);
		Gdx.app.log(SavingPlanetEarth.LOG, "creationTimer : " + creationTimer);
		// take into account the current level of this bonusPack
		float multiplier = getMultiplierAsPerBonusPackType(bonusPackType);
		Gdx.app.log(SavingPlanetEarth.LOG, "multiplier : " + multiplier);
		creationTimer /= multiplier;
		Gdx.app.log(SavingPlanetEarth.LOG, "final creationTimer : "
				+ creationTimer);

		// enque
		enqueBonusPack(creationTimer, bonusPackType);
	}

	private static float getMultiplierAsPerBonusPackType(int bonusPackType) {
		switch (bonusPackType) {
		case BonusPack.HEALTH:
			return MULTIPLIER_TIMER_HEALTH_PACK;
		case BonusPack.STAR:
			return MULTIPLIER_TIMER_STAR_PACK;
		case BonusPack.SHIELD:
			return MULTIPLIER_TIMER_SHIELD_PACK;
		}
		return 1f;
	}

	public static void enqueBonusPack(float creationTimer, int bonusPackType) {
		int bonusPackTypeIndex = purchasedBonusPackTypes.indexOf(bonusPackType,
				false);
		creationTimers.set(bonusPackTypeIndex, creationTimer);
	}
}
