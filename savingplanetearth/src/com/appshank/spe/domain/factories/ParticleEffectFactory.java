package com.appshank.spe.domain.factories;

import com.appshank.spe.utils.ResolutionIndependenceHelper;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;

public class ParticleEffectFactory {

	private enum PEType {
		METEOR, METEOR_EXPLOSION, STAR_ABSORPTION, HEALTH_ABSORPTION;
	}

	private static float scalingFactor;

	private static String PARTICLE_EFFECTS_FOLDER = "particle-effects";

	// initialize the factory variables
	public static void initialize(float stageWidth, float stageHeight) {
		// initialize scalingFactor
		initParticleEffectScalingFactor(stageWidth, stageHeight);
	}

	private static void initParticleEffectScalingFactor(float stageWidth,
			float stageHeight) {
		if (!ResolutionIndependenceHelper.initialized) {
			new ResolutionIndependenceHelper(stageWidth, stageHeight);
		}

		scalingFactor = ResolutionIndependenceHelper.getScalingFactor();
	}

	private static ParticleEffect getPE(PEType type) {
		ParticleEffect effect = new ParticleEffect();

		switch (type) {
		case METEOR:
			effect.load(
					Gdx.files.internal(PARTICLE_EFFECTS_FOLDER
							+ "/meteor_test3.p"),
					Gdx.files.internal(PARTICLE_EFFECTS_FOLDER));
			break;
		case METEOR_EXPLOSION:
			effect.load(
					Gdx.files.internal(PARTICLE_EFFECTS_FOLDER
							+ "/meteor_collision.p"),
					Gdx.files.internal(PARTICLE_EFFECTS_FOLDER));
			break;
		case STAR_ABSORPTION:
			effect.load(
					Gdx.files.internal(PARTICLE_EFFECTS_FOLDER
							+ "/star_absorbed.p"),
					Gdx.files.internal(PARTICLE_EFFECTS_FOLDER));
			break;
		case HEALTH_ABSORPTION:
			// TODO
			break;
		default:
			throw new IllegalArgumentException(
					"Unknown type of Particle Effect sought !");
		}

		scalePE(effect);
		return effect;
	}

	private static void scalePE(ParticleEffect effect) {
		float highMin = effect.getEmitters().get(0).getScale().getHighMin();
		effect.getEmitters().get(0).getScale()
				.setHighMin(highMin * scalingFactor);

		float highMax = effect.getEmitters().get(0).getScale().getHighMax();
		effect.getEmitters().get(0).getScale()
				.setHighMax(highMax * scalingFactor);

	}

	public static ParticleEffect getMeteorPE() {
		return getPE(PEType.METEOR);
	}

	public static ParticleEffect getMeteorExplosionPE() {
		return getPE(PEType.METEOR_EXPLOSION);
	}

	public static ParticleEffect getStarAbsorptionPE() {
		return getPE(PEType.STAR_ABSORPTION);
	}
}
