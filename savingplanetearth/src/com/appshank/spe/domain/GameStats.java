package com.appshank.spe.domain;

/**
 * Class to hold the statistics about the current Game
 * 
 * @author AppShank
 * 
 */
public class GameStats {

	// Time played
	public float gameDuration;

	// total no. of bonus packs appeared
	public int nBonusPacksAppeared;

	// total no. of bonus packs absorbed
	public int nBonusPacksAbsorbed;

	// No. of Health Packs that appeared
	public int nHealthPacksAppeared;

	// No. of Health Packs absorbed
	public int nHealthPacksAbsorbed;

	// No. of Star Packs that appeared
	public int nStarPacksAppeared;

	// No. of Star Packs absorbed
	public int nStarPacksAbsorbed;

	// No. of Shields that appeared
	public int nShieldPacksAppeared;

	// No. of Shields absorbed
	public int nShieldPacksAbsorbed;

	// Score
	public float score;

	public GameStats() {
	}

	public void addBonusPackAppeared(int type) {
		switch (type) {
		case BonusPack.HEALTH:
			nHealthPacksAppeared++;
			break;
		case BonusPack.STAR:
			nStarPacksAppeared++;
			break;
		case BonusPack.SHIELD:
			nShieldPacksAppeared++;
			break;
		}
		nBonusPacksAppeared++;
	}

	public void addBonusPackAbsorbed(int type) {
		switch (type) {
		case BonusPack.HEALTH:
			nHealthPacksAbsorbed++;
			break;
		case BonusPack.STAR:
			nStarPacksAbsorbed++;
			break;
		case BonusPack.SHIELD:
			nShieldPacksAbsorbed++;
			break;
		}
		nBonusPacksAbsorbed++;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("---GameStats---");
		sb.append("\n");
		sb.append("gameDuration : " + gameDuration + " seconds");
		sb.append("\n");
		sb.append("nBonusPacksAppeared : " + nBonusPacksAppeared);
		sb.append("\n");
		sb.append("nBonusPacksAbsorbed : " + nBonusPacksAbsorbed);
		sb.append("\n");
		sb.append("nHealthPacksAppeared : " + nHealthPacksAppeared);
		sb.append("\n");
		sb.append("nHealthPacksAbsorbed : " + nHealthPacksAbsorbed);
		sb.append("\n");
		sb.append("nStarPacksAppeared : " + nStarPacksAppeared);
		sb.append("\n");
		sb.append("nStarPacksAbsorbed : " + nStarPacksAbsorbed);
		sb.append("\n");
		sb.append("nShieldPacksAppeared : " + nShieldPacksAppeared);
		sb.append("\n");
		sb.append("nShieldPacksAbsorbed : " + nShieldPacksAbsorbed);
		return sb.toString();
	}

	public void reset() {
		// Time played
		gameDuration = 0f;

		// total no. of bonus packs appeared
		nBonusPacksAppeared = 0;

		// total no. of bonus packs absorbed
		nBonusPacksAbsorbed = 0;

		// No. of Health Packs that appeared
		nHealthPacksAppeared = 0;

		// No. of Health Packs absorbed
		nHealthPacksAbsorbed = 0;

		// No. of Star Packs that appeared
		nStarPacksAppeared = 0;

		// No. of Star Packs absorbed
		nStarPacksAbsorbed = 0;

		// No. of Shields that appeared
		nShieldPacksAppeared = 0;

		// No. of Shields absorbed
		nShieldPacksAbsorbed = 0;

		// Score
		score = 0f;
	}

}
