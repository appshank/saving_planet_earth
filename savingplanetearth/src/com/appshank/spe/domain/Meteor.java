package com.appshank.spe.domain;

/**
 * A meteor that wants to destroy our planet
 * <p>
 * 
 * @author AppShank
 * 
 */
public enum Meteor {

	REGULAR(50), HUGE(50);

	private static float DIA_REGULAR = 32;
	private static float DIA_HUGE = 32;

	/**
	 * the damage that this meteor does to the planet
	 */
	private int damage;

	private Meteor(int damage) {
		this.damage = damage;
	}

	public int getDamage() {
		return damage;
	}

	public float getDia() {
		if (this == REGULAR)
			return DIA_REGULAR;
		if (this == HUGE)
			return DIA_HUGE;
		return DIA_REGULAR;
	}

	public String getSimpleName() {
		return name().replaceAll("_", "-").toLowerCase();
	}

	/**
	 * return a string of the form "RegularMeteor, damage : 25"
	 */
	@Override
	public String toString() {
		return name() + " METEOR, damage : " + getDamage();
	}

	public static void initDia(float scalingFactor) {
		DIA_REGULAR *= scalingFactor;
		DIA_HUGE *= scalingFactor;
	}

}
