package com.appshank.spe.domain;

import com.badlogic.gdx.math.MathUtils;

/**
 * The planet which is to be saved !!
 * <p>
 * 
 * @author AppShank
 * 
 */
public enum PlanetModel implements Item {

	EARTH("Earth", 100);

	private static final float DIA_EARTH = 100;

	private final String name;
	private final int maxHealth;
	private int health;

	private PlanetModel(String name, int health) {
		this.name = name;
		this.maxHealth = health;
		this.health = health;
	}

	public String getSimpleName() {
		return name().replaceAll("_", "-").toLowerCase();
	}

	/**
	 * return a string of the form "EARTH, maxHealth : 100, health : 70"
	 */
	@Override
	public String toString() {
		return name() + ", maxHealth : " + getMaxHealth() + ", health : "
				+ getHealth();
	}

	// GETTERS FOR PROPERTIES

	public int getMaxHealth() {
		return maxHealth;
	}

	public int getHealth() {
		return health;
	}

	public String getName() {
		return name;
	}

	public float getDia() {
		if (this == EARTH)
			return DIA_EARTH;
		return DIA_EARTH;
	}

	/**
	 * decrease the health by specified amount
	 */
	public void decreaseHealth(int decrementValue) {
		health -= decrementValue;
		health = MathUtils.clamp(health, 0, maxHealth);
	}

	public void resetHealth() {
		health = maxHealth;
	}
}
