package com.appshank.spe.domain;

/**
 * The health pack to increase planet's health
 * <p>
 * 
 * @author AppShank
 * 
 */
public class HealthPack implements BonusPack {

	private static final int DEFAULT_HEALTH_BONUS = 25;
	private static final int PRICE = 0;
	private static final int UPGRADE_COST = 100;

	/**
	 * the health bonus that this pack provides
	 */
	private int healthBonus;

	public HealthPack() {
		this.healthBonus = DEFAULT_HEALTH_BONUS;
	}

	@Override
	public int getType() {
		return BonusPack.HEALTH;
	}

	public int getHealthBonus() {
		return healthBonus;
	}

	@Override
	public int getPrice() {
		return PRICE;
	}

	@Override
	public boolean equals(Object obj) {
		return ((BonusPack) obj).getType() == getType() ? true : false;
	}

	@Override
	public int getUpgradeCost() {
		return UPGRADE_COST;
	}

}
