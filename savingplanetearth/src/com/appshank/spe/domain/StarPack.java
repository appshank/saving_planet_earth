package com.appshank.spe.domain;

public class StarPack implements BonusPack {

	private static final int DEFAULT_SCORE_BONUS = 100;
	private static final int PRICE = 0;
	private static final int UPGRADE_COST = 100;

	/**
	 * the score bonus that this pack provides
	 */
	private int scoreBonus;

	public StarPack() {
		this.scoreBonus = DEFAULT_SCORE_BONUS;
	}

	@Override
	public int getType() {
		return BonusPack.STAR;
	}

	@Override
	public int getPrice() {
		return PRICE;
	}

	public int getScoreBonus() {
		return scoreBonus;
	}

	@Override
	public boolean equals(Object obj) {
		return ((BonusPack) obj).getType() == getType() ? true : false;
	}
	

	@Override
	public int getUpgradeCost() {
		return UPGRADE_COST;
	}
}
