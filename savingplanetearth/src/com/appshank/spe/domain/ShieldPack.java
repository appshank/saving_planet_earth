package com.appshank.spe.domain;

/**
 * The Shield bonus pack
 * <p>
 * 
 * @author AppShank
 * 
 */
public class ShieldPack implements BonusPack {

	private static final int PRICE = 1000;
	private static final int UPGRADE_COST = 100;

	/**
	 * The shield that gets attached to the planet on absorbing this pack
	 */
	private Shield shield;

	public ShieldPack() {
		// DEFAULT SHIELD
		this.shield = Shield.MS;
	}

	@Override
	public int getType() {
		return BonusPack.SHIELD;
	}

	@Override
	public int getPrice() {
		return PRICE;
	}

	@Override
	public boolean equals(Object obj) {
		return ((BonusPack) obj).getType() == getType() ? true : false;
	}

	public Shield getShield() {
		return shield;
	}

	@Override
	public int getUpgradeCost() {
		return UPGRADE_COST;
	}

}
