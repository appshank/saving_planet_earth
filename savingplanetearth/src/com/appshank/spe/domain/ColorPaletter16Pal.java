package com.appshank.spe.domain;

import com.badlogic.gdx.graphics.Color;

public class ColorPaletter16Pal {

	public static Color VOID = new Color(0f, 0f, 0f, 1f);
	public static Color ASH = new Color(157f / 255f, 157f / 255f, 157f / 255f,
			1f);
	public static Color BLIND = new Color(1f, 1f, 1f, 1f);
	public static Color BLOODRED = new Color(190f / 255f, 38f / 255f,
			51f / 255f, 1f);
	public static Color PIGMEAT = new Color(224f / 255f, 111f / 255f,
			139f / 255f, 1f);
	public static Color OLDPOOP = new Color(73f / 255f, 60f / 255f, 43f / 255f,
			1f);
	public static Color NEWPOOP = new Color(164f / 255f, 100f / 255f,
			34f / 255f, 1f);
	public static Color BLAZE = new Color(235f / 255f, 137f / 255f, 49f / 255f,
			1f);
	public static Color ZORNSKIN = new Color(247f / 255f, 226f / 255f,
			107f / 255f, 1f);
	public static Color SHADEGREEN = new Color(47f / 255f, 72f / 255f,
			78f / 255f, 1f);
	public static Color LEAFGREEN = new Color(68f / 255f, 137f / 255f,
			26f / 255f, 1f);
	public static Color SLIMEGREEN = new Color(163f / 255f, 206f / 255f,
			39f / 255f, 1f);
	public static Color NIGHTBLUE = new Color(27f / 255f, 38f / 255f,
			50f / 255f, 1f);
	public static Color SEABLUE = new Color(0f, 87f / 255f, 132f / 255f, 1f);
	public static Color SKYBLUE = new Color(49f / 255f, 162f / 255f,
			242f / 255f, 1f);
	public static Color CLOUDBLUE = new Color(178f / 255f, 220f / 255f,
			239f / 255f, 1f);

}
