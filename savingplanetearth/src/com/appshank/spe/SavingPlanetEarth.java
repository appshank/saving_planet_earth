package com.appshank.spe;

import com.appshank.spe.domain.GameStats;
import com.appshank.spe.screens.HighScoresScreen;
import com.appshank.spe.screens.MenuScreen;
import com.appshank.spe.screens.OptionsScreen;
import com.appshank.spe.screens.SplashScreen;
import com.appshank.spe.services.MusicManager;
import com.appshank.spe.services.MyAssetsManager;
import com.appshank.spe.services.PreferencesManager;
import com.appshank.spe.services.ProfileManager;
import com.appshank.spe.services.SoundManager;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;

public class SavingPlanetEarth extends Game {

	// constant useful for logging
	public static final String LOG = "SavingPlanetEarth";

	private static volatile SavingPlanetEarth instance = null;

	// libgdx helper class for logging fps
	protected FPSLogger fpsLogger;

	// whether we are in development mode
	// set this to false before releasing
	public static final boolean DEV_MODE = true;

	// the stats for the game
	public GameStats gameStats;

	private boolean reResize = true;

	// services
	protected PreferencesManager preferencesManager;
	protected ProfileManager profileManager;
	protected MusicManager musicManager;
	protected SoundManager soundManager;
	protected MyAssetsManager assetsManager;

	public static SavingPlanetEarth getInstance() {
		if (instance == null) {
			synchronized (SavingPlanetEarth.class) {
				if (instance == null) {
					instance = new SavingPlanetEarth();
				}
			}
		}
		return instance;
	}

	public SavingPlanetEarth() {
		super();
		gameStats = new GameStats();
	}

	// Screen methods

	// Screen methods

	public SplashScreen getSplashScreen() {
		return new SplashScreen(this);
	}

	public HighScoresScreen getHighScoresScreen() {
		return new HighScoresScreen(this);
	}

	public MenuScreen getMenuScreen() {
		return new MenuScreen(this);
	}

	public OptionsScreen getOptionsScreen() {
		return new OptionsScreen(this);
	}

	@Override
	public void create() {

		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		Gdx.app.log(SavingPlanetEarth.LOG,
				"Creating game on " + Gdx.app.getType());

		// create the profile manager
		profileManager = new ProfileManager();

		// create the preferences manager
		preferencesManager = new PreferencesManager();

		// create the music manager
		musicManager = new MusicManager();
		musicManager.setVolume(preferencesManager.getVolume());
		musicManager.setEnabled(preferencesManager.isMusicEnabled());

		// create the sound manager
		soundManager = new SoundManager();
		soundManager.setVolume(preferencesManager.getVolume());
		soundManager.setEnabled(preferencesManager.isSoundEnabled());

		// create the assets manager
		assetsManager = new MyAssetsManager();
		assetsManager.init();

		reResize = false;

		// create the helper objects
		fpsLogger = new FPSLogger();

	}

	@Override
	public void render() {
		super.render();

		// output the current FPS
		if (DEV_MODE)
			fpsLogger.log();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		Gdx.app.log(SavingPlanetEarth.LOG, "Resizing game to: " + width + " x "
				+ height);

		// show the splash screen when the game is resized for the first time;
		// this approach avoids calling the screen's resize method repeatedly
		setScreen(getSplashScreen());
	}

	@Override
	public void pause() {
		Gdx.app.log(LOG, "Pausing game");
		super.pause();

		// saving the profile because we dont know if the player will return to
		// the game
		getProfileManager().persist();
	}

	@Override
	public void resume() {
		Gdx.app.log(LOG, "Resuming game");
	}

	@Override
	public void dispose() {
		super.dispose();
		Gdx.app.log(SavingPlanetEarth.LOG, "Disposing game");

		// dispose some services and other assets
		// protected PreferencesManager preferencesManager;
		// protected ProfileManager profileManager;
		musicManager.dispose();
		soundManager.dispose();
		assetsManager.dispose();
	}

	public PreferencesManager getPreferencesManager() {
		return preferencesManager;
	}

	public MusicManager getMusicManager() {
		return musicManager;
	}

	public SoundManager getSoundManager() {
		return soundManager;
	}

	public ProfileManager getProfileManager() {
		return profileManager;
	}

	public MyAssetsManager getAssetsManager() {
		return assetsManager;
	}
}
