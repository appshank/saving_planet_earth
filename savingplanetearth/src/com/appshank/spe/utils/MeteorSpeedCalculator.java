package com.appshank.spe.utils;

/**
 * calculates the speeds of the meteors on basis of the device resolution on the
 * basis of the base speeds
 * 
 * @author AppShank
 * 
 */
public class MeteorSpeedCalculator {

	private static class BaseMeteorSpeeds {
		// the default initial speed of the meteor
		private static final float MIN_INITIAL_SPEED = 28f;
		private static final float MAX_INITIAL_SPEED = 28.5f;
		private static final float INCREMENT = 0.8f;
		private static final float SMALL_INCREMENT = 0.5f; // after
															// MAX_SPEED_KNEE1
		private static final float MINI_INCREMENT = 0.3f; // after
															// MAX_SPEED_KNEE2
		private static final float MICRO_INCREMENT = 0.1f; // after
															// MAX_SPEED_KNEE3
		private static final float MAX_SPEED_KNEE1 = 29f;
		private static final float MAX_SPEED_KNEE2 = 31f;
		private static final float MAX_SPEED_KNEE3 = 33f;
		private static final float MAX_SPEED_ABS = 35f;

		// speed limit increment per thousand score
		private static final float INCREMENT_PER_THOUSAND = 2f;
	}

	// the factor with which the base speeds need to be multiplied
	final float scalingFactor;

	public MeteorSpeedCalculator(float stageWidth, float stageHeight) {
		if (!ResolutionIndependenceHelper.initialized) {
			new ResolutionIndependenceHelper(stageWidth, stageHeight);
		}
		scalingFactor = ResolutionIndependenceHelper.getScalingFactor();
	}

	public float getMinInitialSpeed() {
		return scalingFactor * BaseMeteorSpeeds.MIN_INITIAL_SPEED;
	}

	public float getMaxInitialSpeed() {
		return scalingFactor * BaseMeteorSpeeds.MAX_INITIAL_SPEED;
	}

	public float getIncrement() {
		return scalingFactor * BaseMeteorSpeeds.INCREMENT;
	}

	public float getSmallIncrement() {
		return scalingFactor * BaseMeteorSpeeds.SMALL_INCREMENT;
	}

	public float getMiniIncrement() {
		return scalingFactor * BaseMeteorSpeeds.MINI_INCREMENT;
	}

	public float getMicroIncrement() {
		return scalingFactor * BaseMeteorSpeeds.MICRO_INCREMENT;
	}

	public float getMaxSpeedKnee1() {
		return scalingFactor * BaseMeteorSpeeds.MAX_SPEED_KNEE1;
	}

	public float getMaxSpeedKnee2() {
		return scalingFactor * BaseMeteorSpeeds.MAX_SPEED_KNEE2;
	}

	public float getMaxSpeedKnee3() {
		return scalingFactor * BaseMeteorSpeeds.MAX_SPEED_KNEE3;
	}

	public float getMaxSpeedAbs() {
		return scalingFactor * BaseMeteorSpeeds.MAX_SPEED_ABS;
	}

	public float getIncrementPerThousand() {
		return scalingFactor * BaseMeteorSpeeds.INCREMENT_PER_THOUSAND;
	}

}
