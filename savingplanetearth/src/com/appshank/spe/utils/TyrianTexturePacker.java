package com.appshank.spe.utils;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2.Settings;

/**
 * Packs single images into image atlases.
 */
public class TyrianTexturePacker {

	private static final String INPUT_DIR = "etc/images";
	private static final String OUTPUT_DIR = "../savingplanetearth_android/assets/image-atlases";
	private static final String PACK_FILE = "pages";

	private static final String[] RESOLUTION_FOLDERS = { "480800", "6001024",
			"8001280" };

	public static void main(String[] args) {
		// create the packing's settings
		Settings settings = new Settings();

		// adjust the padding settings
		settings.edgePadding = false;
		settings.filterMin = TextureFilter.Linear;
		settings.filterMag = TextureFilter.Linear;

		// set the maximum dimension of each image atlas
		settings.maxWidth = 2048;
		settings.maxHeight = 1024;

		// pack the images
		for (String resolution_folder : RESOLUTION_FOLDERS) {
			TexturePacker2.process(settings, INPUT_DIR + "/"+resolution_folder,
					OUTPUT_DIR + "/"+resolution_folder, PACK_FILE);
		}

	}

}
