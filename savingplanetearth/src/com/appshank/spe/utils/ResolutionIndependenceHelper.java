package com.appshank.spe.utils;

public class ResolutionIndependenceHelper {

	private static class BaseDimensions {
		static int baseWidth = 800;
		static int baseHeight = 480;
		static float baseDiagonal = (float) Math.sqrt(baseWidth * baseWidth
				+ baseHeight * baseHeight);
	}

	// indicated whether this helper has been initialized or not
	public static boolean initialized = false;

	private float diagonal;

	// the factor with which the base speeds need to be multiplied
	private static float scalingFactor;

	public ResolutionIndependenceHelper(float stageWidth, float stageHeight) {
		diagonal = (float) Math.sqrt(stageWidth * stageWidth + stageHeight
				* stageHeight);
		scalingFactor = diagonal / BaseDimensions.baseDiagonal;
		initialized = true;
	}

	public static float getScalingFactor() {
		return scalingFactor;
	}
}
