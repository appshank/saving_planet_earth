package com.appshank.spe.utils;

/**
 * An enumeration of the screen edges
 * <p>
 * 
 * @author AppShank
 * 
 */
public enum Corner {
	TOP_LEFT, BOTTOM_LEFT, TOP_RIGHT, BOTTOM_RIGHT;
}
