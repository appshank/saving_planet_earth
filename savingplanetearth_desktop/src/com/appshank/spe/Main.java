package com.appshank.spe;

import org.lwjgl.opengl.Display;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

class Focus extends SavingPlanetEarth {
	private boolean wasPaused = false;

	@Override
	public void render() {
		super.render();
		if (!Display.isActive() && !wasPaused) {
			this.getScreen().pause();
			wasPaused = true;
		}
		if (wasPaused && Display.isActive()) {
			wasPaused = false;
		}
	}
}

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "saving-planet-earth";
		cfg.useGL20 = false;
		cfg.width = 800;
		cfg.height = 480;

		cfg.forceExit = false;// stop jvm crashes on app exit

		new LwjglApplication(new Focus(), cfg);
	}
}
